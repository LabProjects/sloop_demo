/*
 * pof_byte_transfer.h
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "sloop/include/global_var.h"
#ifndef POP_METADATA_SLOOP_SOURCE_POF_BYTE_TRANSFER_H_
#define POP_METADATA_SLOOP_SOURCE_POF_BYTE_TRANSFER_H_
uint32_t pof_NtoH_transfer_header(void *ptr);
uint32_t pof_HtoN_transfer_header(void *ptr);
uint32_t pof_NtoH_transfer_trace(void*ptr);
uint32_t trace_node_NtoH_transfer(void*ptr);
uint32_t pof_HtoN_transfer_feature_reply(void *ptr);
uint32_t pof_HtoN_transfer_protocol_reply(void*ptr);
uint32_t pof_HtoN_transfer_policy_reply(void *ptr);
uint32_t pof_HtoN_transfer_FDT_reply(void* ptr);
uint32_t pof_NtoH_trasfer_set_protocol(void*ptr);
uint32_t pof_NtoH_transfer_protocol_header(void* ptr);
uint32_t pof_HtoN_transfer_hostReport(void* ptr);
uint32_t pof_HtoN_transfer_switchReport(void* ptr);
uint32_t pof_HtoN_transfer_linkReport(void* ptr);

//???????????????????????????????
//uint32_t pof_HtoN_transfer_llldpReport(void* ptr);
#endif /* POP_METADATA_SLOOP_SOURCE_POF_BYTE_TRANSFER_H_ */
