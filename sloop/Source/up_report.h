/*
 * up_report.h
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "global_var.h"
#include "msg_header.h"
#ifndef POP_METADATA_SLOOP_SOURCE_UP_REPORT_H_
#define POP_METADATA_SLOOP_SOURCE_UP_REPORT_H_
uint32_t hostReport(uint8_t reason,uint32_t dpid,uint32_t portId,uint32_t hostId,uint32_t hostIp);
uint32_t switchReport(uint8_t reason,uint32_t dpid,uint8_t portNum,uint8_t* portReasons,struct PortReport * port);
uint32_t linkReport(uint32_t dstPort,uint32_t dstSwitch,uint32_t srcPort,uint32_t srcSwitch,uint8_t linkstate,uint8_t linktype,uint8_t reason);
uint32_t lldpReport();

#endif /* POP_METADATA_SLOOP_SOURCE_UP_REPORT_H_ */
