/*
 * task.c
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "sloop/Source/task.h"

#include  <pthread.h>
#include <unistd.h>

#include "sloop/include/global_var.h"
#include "sloop/include/msg_header.h"
#include "sloop/include/param.h"
#include "sloop/Source/base_func.h"
#include "sloop/Source/msg.h"
#include "sloop/Source/process_msg.h"
#include "sloop/Source/xlog.h"
extern conn_desc core_conn_desc;
extern uint32_t g_upward_xid;
extern uint32_t send_queue_id;
//#include <pthread.h>
uint32_t task_create(void *arg, TASK_FUNC task_func, task_t *task_id_ptr){
    if(NULL == task_func || NULL == task_id_ptr){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TASK_CREATE_FAIL);
    	return TASK_ERROR;
    }

    if(RET_OK != pthread_create((task_t *)task_id_ptr, NULL, (void *)task_func, arg)){
        *task_id_ptr = INVALID_TASKID;
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TASK_CREATE_FAIL);
        return TASK_ERROR;
    }


    return RET_OK;
}

uint32_t task_delay(uint32_t delay){
    /* The unit of the argument of usleep function is micro-seconds. */
    usleep(delay*1000);
    return 0;
}

uint32_t task_delete(task_t *task_id_ptr){
    task_t task_id = INVALID_TASKID;

    if(NULL == task_id_ptr){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TASK_DELETE_FAIL);
    }
    task_id = *task_id_ptr;

    if(task_id == INVALID_TASKID){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TASK_DELETE_FAIL);
    }
    if(task_id == pthread_self()){
        return RET_OK;
    }

    if(RET_OK != pthread_cancel(task_id)){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TASK_DELETE_FAIL);
    }

    *task_id_ptr = INVALID_TASKID;

    return RET_OK;
}

uint32_t main_task(void *arg_ptr)
{
	    conn_desc *conn_desc_ptr = (conn_desc *)&core_conn_desc;
	    struct core_header* head_ptr, head;
	    int total_len = 0, tmp_len=0, left_len=0, rcv_len = 0, process_len = 0, packet_len = 0;
	    int socket_fd;
	    uint32_t ret;
	    xinfo("***********MAIN_TASK IS START*********************\n");
	    /* State machine for deal with the  channel of core. */
	    while(1)
	    {

	        switch(conn_desc_ptr->conn_status){
	            case CHANNEL_INVALID:

	                /* Create  channel socket. */
	                ret = core_create_socket(&socket_fd);
	                if(ret == RET_OK){
	                    conn_desc_ptr->sfd = socket_fd;
	                    conn_desc_ptr->conn_status = CHANNEL_CONNECTING;
	                }else{
	                    printf("Created socket failure\n");
	                }
	                break;

	            case CHANNEL_CONNECTING:
	                /* Connect core. */

	                ret = connect_core(conn_desc_ptr->sfd, conn_desc_ptr->core_ip,conn_desc_ptr->core_port);
	                if(ret == RET_OK){
	                    conn_desc_ptr->conn_status= CHANNEL_CONNECTED;
	                    conn_desc_ptr->retry_count = 0;
	                }else{
						//whether add many times connect retry

	                	if(!conn_desc_ptr->retry_count){
	                			//POF_DEBUG_CPRINT_FL(1,RED,">>Connect to controler FAIL!");
	                	}
	                	 /* Delay several seconds. */
	                	 task_delay(conn_desc_ptr->retry_interval * 1000);
	                	 conn_desc_ptr->retry_count++;
	                	 //conn_desc_ptr->conn_status.last_error = (uint8_t)(POF_CONNECT_SERVER_FAILURE); /**/
	                	 conn_desc_ptr->sfd = 0;
	                	 conn_desc_ptr->conn_status = CHANNEL_INVALID;
	                }
	                break;

	            case CHANNEL_CONNECTED:
	                /* Send hello to core. Hello message has no body. */
	                build_header(&head, HELLO, sizeof(struct core_header), g_upward_xid++);
	                /* send hello message. */
	                ret = msg_send(conn_desc_ptr->sfd, (char*)&head, sizeof(struct core_header));
	                if(ret == RET_OK){
	                    conn_desc_ptr->conn_status = WAIT_HELLO;
	                }else{
	                    printf("Send HELLO FAIL!");
	                }

	                break;

	            case WAIT_HELLO:
	                /* Receive hello from core. */
	                total_len = 0;
	                left_len = 0;
	                rcv_len = 0;
	                process_len = 0;
	                ret = msg_recv(conn_desc_ptr->sfd, conn_desc_ptr->recv_buf , RECV_BUF_MAX_SIZE, &total_len);
	                if(ret == RET_OK){
	                    printf(">>Recevie HELLO packet SUC!\n");
	                    conn_desc_ptr->conn_status = FEATURE_REQUEST;
	                }else{
	                    printf("Recv HELLO FAILE!");
	                    break;
	                }
	                rcv_len += total_len;

	                /* Parse message. */
	                head_ptr = (struct core_header*)(conn_desc_ptr->recv_buf);
	                while(total_len < POF_NTOHS(head_ptr->length)){
	                    ret = msg_recv(conn_desc_ptr->sfd, conn_desc_ptr->recv_buf  + rcv_len, RECV_BUF_MAX_SIZE -rcv_len,  &tmp_len);
	                    if(ret != RET_OK){
	                        printf("Recv HELLO FAILE!\n");
	                        break;
	                    }

	                    total_len += tmp_len;
	                    rcv_len += tmp_len;
	                }

	                if(conn_desc_ptr->conn_status == CHANNEL_INVALID){
	                    break;
	                }

	                /* Check whether exist any error. */
	                if(head_ptr->version > CORE_VERSION){
	                    printf("Version of recv-packet is higher than support!\n");
	                    close(conn_desc_ptr->sfd);
	                    conn_desc_ptr->conn_status = CHANNEL_INVALID;
	                }else if(head_ptr->type != HELLO){
	                    printf("Type of recv-packet is not HELLO, which we want to recv!\n");
	                    close(conn_desc_ptr->sfd);
	                    conn_desc_ptr->conn_status= CHANNEL_INVALID;
	                }

	                process_len += POF_NTOHS(head_ptr->length);
	                left_len = rcv_len - process_len;
	                if(left_len == 0){
	                    rcv_len = 0;
	                    process_len = 0;
	                }
	                break;
	            case FEATURE_REQUEST:
	            	 head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf  + process_len);
	            	 if(!((left_len >= sizeof(struct core_header))&&(left_len >= POF_NTOHS(head_ptr->length)))){
	            	       ret = msg_recv(conn_desc_ptr->sfd, (conn_desc_ptr->recv_buf  + rcv_len), \
	            	                            RECV_BUF_MAX_SIZE - rcv_len, &total_len);
	            	        if(ret == RET_OK){
	            	             conn_desc_ptr->conn_status = WAIT_SET_PROTO;
	            	             }else{
	            	                  //POF_ERROR_CPRINT_FL("Feature request FAIL!");
	            	                  break;
	            	              }

	            	             rcv_len += total_len;
	            	             total_len += left_len;

	            	             head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf + process_len);
	            	             while(total_len < POF_NTOHS(head_ptr->length)){
	            	                   ret = msg_recv(conn_desc_ptr->sfd, ((conn_desc_ptr->recv_buf  + rcv_len)), \
	            	                         RECV_BUF_MAX_SIZE-rcv_len ,&tmp_len);
	            	                        if(ret != RET_OK){
	            	                           // POF_ERROR_CPRINT_FL("Feature request FAIL!");
	            	                            break;
	            	                        }
	            	                        total_len += tmp_len;
	            	                        rcv_len += tmp_len;
	            	                    }
	            	       }

	            	    if(conn_desc_ptr->conn_status == CHANNEL_INVALID){
	            	                    break;
	            	          }

	            	    head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf  + process_len);

	            	          /* Check any error. */
	            	     if(head_ptr->type != FEATURE_REQUEST){
	            	            close(conn_desc_ptr->sfd);
	            	            conn_desc_ptr->conn_status = CHANNEL_INVALID;
	            	             break;
	            	        }

	            	     // POF_DEBUG_CPRINT_FL(1,GREEN,">>Recevie FEATURE_REQUEST packet SUC!");
	            	     conn_desc_ptr->conn_status = WAIT_SET_PROTO;
	            	     packet_len = POF_NTOHS(head_ptr->length);

	            	     ret = parse_msg_from_core(conn_desc_ptr->recv_buf + process_len);

	            	     if(ret != RET_OK){
	            	           //POF_ERROR_CPRINT_FL("Features request FAIL!");
	            	           //terminate_handler();
	            	           break;
	            	        }

	            	     process_len += packet_len;
	            	     left_len = rcv_len - process_len;
	            	     if(left_len == 0){
	            	             rcv_len = 0;
	            	             process_len = 0;
	            	       }

	            	     break;
	            case WAIT_SET_PROTO:
	                /* Receive set_proto message from core. */
	                head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf  + process_len);
	                if(!((left_len >= sizeof(struct core_header))&&(left_len >= POF_NTOHS(head_ptr->length)))){
	                    ret = msg_recv(conn_desc_ptr->sfd, (conn_desc_ptr->recv_buf  + rcv_len),RECV_BUF_MAX_SIZE - rcv_len, &total_len);
	                    if(ret == RET_OK){
	                        conn_desc_ptr->conn_status = WAIT_SET_FUN;
	                    }else{
	                        printf("Set PROTO FAIL!");
	                        break;
	                    }

	                    rcv_len += total_len;
	                    total_len += left_len;

	                    head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf + process_len);
	                    while(total_len < POF_NTOHS(head_ptr->length)){
	                        ret = msg_recv(conn_desc_ptr->sfd, ((conn_desc_ptr->recv_buf  + rcv_len)),RECV_BUF_MAX_SIZE-rcv_len ,&tmp_len);
	                        if(ret != RET_OK){
	                            //POF_ERROR_CPRINT_FL("Set PROTO FAIL!");
	                            break;
	                        }
	                        total_len += tmp_len;
	                        rcv_len += tmp_len;
	                    }
	                }

	                if(conn_desc_ptr->conn_status == CHANNEL_INVALID){
	                    break;
	                }

	                head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf  + process_len);

	                /* Check any error. */
	                if(head_ptr->version > CORE_VERSION){
	                    printf("Version of recv-packet is higher than support!");
	                    printf("Set config FAIL!");
	                    close(conn_desc_ptr->sfd);
	                    conn_desc_ptr->conn_status= CHANNEL_INVALID;
	                    break;
	                }else if(head_ptr->type != WAIT_SET_PROTO){
	                	printf("Type of recv-packet is not SET_CONFIG, which we want to recv!");
	                	printf("Set config FAIL!");
	                    close(conn_desc_ptr->sfd);
	                    conn_desc_ptr->conn_status= CHANNEL_INVALID;
	                    break;
	                }

	                printf(">>Recevie SET_CONFIG packet SUC!");
	                conn_desc_ptr->conn_status = WAIT_SET_FUN;
	                packet_len = POF_NTOHS(head_ptr->length);

	                ret = parse_msg_from_core(conn_desc_ptr->recv_buf + process_len);

	                if(ret != RET_OK){
	                	printf("Set config FAIL!");
	                    break;
	                }

	                process_len += packet_len;
	                left_len = rcv_len - process_len;

	                if(left_len == 0){
	                    rcv_len = 0;
	                    process_len = 0;
	                }
	                break;

	            case WAIT_SET_FUN:
	            	/* Receive set_fun message from core. */
	            	head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf  + process_len);
	            	if(!((left_len >= sizeof(struct core_header))&&(left_len >= POF_NTOHS(head_ptr->length)))){
	            		ret = msg_recv(conn_desc_ptr->sfd, (conn_desc_ptr->recv_buf  + rcv_len),RECV_BUF_MAX_SIZE - rcv_len, &total_len);
	            		if(ret == RET_OK)
	            		{
	            	       conn_desc_ptr->conn_status = WAIT_SET_FUN;
	            	     }
	            		else
	            		{
	            	           printf("Set FUN FAIL!");
	            	           break;
	            	     }

	            	    rcv_len += total_len;
	            	    total_len += left_len;

	            	    head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf + process_len);
	            	    while(total_len < POF_NTOHS(head_ptr->length)){
	            	          ret = msg_recv(conn_desc_ptr->sfd, ((conn_desc_ptr->recv_buf  + rcv_len)),RECV_BUF_MAX_SIZE-rcv_len ,&tmp_len);
	            	          if(ret != RET_OK){
	            	          //POF_ERROR_CPRINT_FL("Set FUN FAIL!");
	            	          break;
	            	          }
	            	          total_len += tmp_len;
	            	          rcv_len += tmp_len;
	            	          }
	            	       }

	            	    if(conn_desc_ptr->conn_status == CHANNEL_INVALID){
	            	        break;
	            	       }

	            	    head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf  + process_len);

	            	    /* Check any error. */
	            	    if(head_ptr->version > CORE_VERSION){
	            	    printf("Version of recv-packet is higher than support!");
	            	    printf("Set config FAIL!");
	            	    close(conn_desc_ptr->sfd);
	            	    conn_desc_ptr->conn_status= CHANNEL_INVALID;
	            	    break;
	            	    }else if(head_ptr->type != WAIT_SET_FUN){
	            	    printf("Type of recv-packet is not SET_CONFIG, which we want to recv!");
	            	    printf("Set FUN FAIL!");
	            	    close(conn_desc_ptr->sfd);
	            	    conn_desc_ptr->conn_status= CHANNEL_INVALID;
	            	    break;
	            	    }

	            	    printf(">>Recevie SET_FUN packet SUC!");
	            	    conn_desc_ptr->conn_status = WAIT_SET_FUN;
	            	    packet_len = POF_NTOHS(head_ptr->length);

	            	    ret = parse_msg_from_core(conn_desc_ptr->recv_buf + process_len);

	            	    if(ret != RET_OK){
	            	           printf("Set config FAIL!");
	            	           break;
	            	                }

	            	     process_len += packet_len;
	            	     left_len = rcv_len - process_len;

	            	     if(left_len == 0){
	            	           rcv_len = 0;
	            	           process_len = 0;
	            	                }
	            	     break;
	            case CHANNEL_RUN:
	                /* Wait to receive msg from core. */
	                head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf  + process_len);
	                if(!((left_len >= sizeof(struct core_header))&&(left_len >= POF_NTOHS(head_ptr->length)))){
	                    /* Resv_buf has no space, so should move the left data to the head of the buf. */
	                    if(RECV_BUF_MAX_SIZE == rcv_len){
	                        //memcpy(conn_desc_ptr->recv_buf,  conn_desc_ptr->recv_buf  + process_len, left_len);
	                        memmove(conn_desc_ptr->recv_buf,  conn_desc_ptr->recv_buf  + process_len, left_len);
	                        rcv_len = left_len;
	                        process_len = 0;
	                    }
	                    ret = msg_recv(conn_desc_ptr->sfd, (conn_desc_ptr->recv_buf  + rcv_len),RECV_BUF_MAX_SIZE - rcv_len, &total_len);
	                    if(ret != RET_OK){
	                        break;
	                    }

	                    rcv_len += total_len;
	                    total_len += left_len;

	                    head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf + process_len);
	                    while(total_len < POF_NTOHS(head_ptr->length)){
	                        left_len = rcv_len - process_len;
	                        /* Resv_buf has no space, so should move the left data to the head of the buf. */
	                        if(RECV_BUF_MAX_SIZE == rcv_len){
	                            //memcpy(conn_desc_ptr->recv_buf,  conn_desc_ptr->recv_buf  + process_len, left_len);
	                            memmove(conn_desc_ptr->recv_buf,  conn_desc_ptr->recv_buf  + process_len, left_len);
	                            rcv_len = left_len;
	                            process_len = 0;
	                        }

	                        ret = msg_recv(conn_desc_ptr->sfd, ((conn_desc_ptr->recv_buf  + rcv_len)), RECV_BUF_MAX_SIZE-rcv_len ,&tmp_len);
	                        if(ret != RET_OK){
	                            break;
	                        }
	                    total_len += tmp_len;
	                    rcv_len += tmp_len;
	                    }
	                }

	                if(conn_desc_ptr->conn_status == CHANNEL_INVALID){
	                    break;
	                }

	                head_ptr = (struct core_header *)(conn_desc_ptr->recv_buf  + process_len);
	                packet_len = POF_NTOHS(head_ptr->length);
	                /* Handle the message. Echo messages will be processed here and other messages will be forwarded to LUP. */
	                if(head_ptr->type==ECHO_REPLY)
	                	conn_desc_ptr->last_echo_time=time(NULL);
	                else
	                	ret = run_process_msg(conn_desc_ptr->recv_buf + process_len, packet_len);

	                process_len += packet_len;
	                left_len = rcv_len - process_len;

	                if(left_len == 0){
	                    rcv_len = 0;
	                    process_len = 0;
	                }
	                break;

	            default:
	                conn_desc_ptr->conn_status = CHANNEL_WRONG_STATE;
	                break;
	        }

	    }
	    return RET_OK;
}

uint32_t send_msg_task(void *arg_ptr){
    conn_desc *conn_desc_ptr = (conn_desc *)&core_conn_desc;
    struct core_header *head_ptr;
    uint32_t   ret;

    /* Polling the message queue. If valid, fetch one message and send it to controller. */
    xinfo("***********SEND_MSG_TASK START*********************\n");
    while(1){
    	//xinfo("***********SEND_MSG_TASK START*********************\n");
        /* Set the pthread cancel point. */
        //pthread_testcancel();
        switch(conn_desc_ptr->conn_status){
            case CHANNEL_INVALID:
            case CHANNEL_CONNECTING:
            case CHANNEL_CONNECTED:
            case WAIT_HELLO:
                task_delay(100);
                break;
            case FEATURE_REQUEST:
            case WAIT_SET_PROTO:
            case WAIT_SET_FUN:
            case CHANNEL_RUN:
                /* Fetch one message from message queue. */
                ret = msg_queue_read(send_queue_id, conn_desc_ptr->msg_buf, MESSAGE_MAX_SIZE, WAIT_FOREVER);
                if(ret != RET_OK){
                    //pofsc_set_error(POFET_SOFTWARE_FAILED, ret);
                	printf("msg_queue_read_error\n");
                    break;
                }

                /* Send message to server. */
                head_ptr = (struct core_header*)conn_desc_ptr->msg_buf;
                ret = msg_send(conn_desc_ptr->sfd, conn_desc_ptr->msg_buf, POF_HTONS(head_ptr->length));
                if(ret != RET_OK){
                    /* Return to inalid state. */
                    conn_desc_ptr->sfd = 0;
                    conn_desc_ptr->conn_status = CHANNEL_INVALID;

                    /* Put the message back to queue for sendding next time. */
                    ret = msg_queue_write(send_queue_id, conn_desc_ptr->msg_buf, POF_HTONS(head_ptr->length), WAIT_FOREVER);
                    if(ret != RET_OK){
                        //pofsc_set_error(POFET_SOFTWARE_FAILED, ret);
                    	printf("append to the queue error\n");
                        break;
                    }
                }
                break;
            default:
                //conn_desc_ptr->conn_status.last_error = (uint8_t)POF_WRONG_CHANNEL_STATE;
            	conn_desc_ptr->conn_status=CHANNEL_INVALID;
                break;
        }
    }
    uint32_t ok;
    return ok;
}


