/*
 * process_msg.h
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */

#include "sloop/include/global_var.h"
#include "sloop/include/trace_tree.h"
#ifndef POP_METADATA_SLOOP_SOURCE_PROCESS_MSG_H_
#define POP_METADATA_SLOOP_SOURCE_PROCESS_MSG_H_
uint32_t run_process_msg(char *message, uint16_t len);
uint32_t  parse_msg_from_core(char* msg_ptr);
bool add_trace_to_tree(struct trace_tree** root,struct trace_tree** trace,int nodeNum);
uint32_t  reply_msg(uint8_t  type,uint32_t xid,uint32_t msg_len,uint8_t  *msg_body);
uint32_t send_msg_to_queue(uint8_t *msg, uint32_t len);

#endif /* POP_METADATA_SLOOP_SOURCE_PROCESS_MSG_H_ */
