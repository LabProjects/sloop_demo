/*
 * task.h
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "sloop/include/global_var.h"
#ifndef POP_METADATA_SLOOP_SOURCE_TASK_H_
#define POP_METADATA_SLOOP_SOURCE_TASK_H_
uint32_t task_create(void *arg, TASK_FUNC task_func, task_t *task_id_ptr);
uint32_t task_delay(uint32_t delay);
uint32_t task_delete(task_t *task_id_ptr);
uint32_t main_task(void *arg_ptr);
uint32_t send_msg_task(void *arg_ptr);
#endif /* POP_METADATA_SLOOP_SOURCE_TASK_H_ */
