/*
 * msg.h
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "sloop/include/global_var.h"
#ifndef POP_METADATA_SLOOP_SOURCE_MSG_H_
#define POP_METADATA_SLOOP_SOURCE_MSG_H_
uint32_t msg_queue_create(uint32_t *queue_id_ptr);
uint32_t msg_queue_delete(uint32_t *queue_id_ptr);
uint32_t msg_queue_read(uint32_t queue_id, void *buf, uint32_t max_len, int timeout);
uint32_t msg_queue_write(uint32_t queue_id, const void *message, uint32_t msg_len, int timeout);
uint32_t msg_recv(int socket_fd, char* buf, int buflen, int* plen);
uint32_t msg_send(int socket_fd, char* buf, int len);


#endif /* POP_METADATA_SLOOP_SOURCE_MSG_H_ */
