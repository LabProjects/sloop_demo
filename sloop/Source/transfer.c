/*
 * transfer.c
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */


#include <string.h>
#include <assert.h>

#include "sloop/include/action.h"
#include "sloop/include/global_var.h"
#include "sloop/include/msg_header.h"
#include "sloop/include/param.h"
#include "sloop/include/spec_header.h"
#include "sloop/include/trace_tree.h"
#include "sloop/include/value.h"
#include "sloop/Source/transfer.h"
extern struct header * headerList[MAX_HEADER_SUM];

uint32_t transfer_Protocol_from_core_to_native(struct Set_Protocol* pro_ptr)
{
	int index=0;
	int next_index=0;
	int field_index=0;
	int headerNum=pro_ptr->headerNum;
	uint32_t ret= RET_OK;
	for(index=0;index<headerNum;index++)
	{
		struct header* headerptr=malloc(sizeof(struct header));
		headerList[index]=headerptr;
	}
	for(index=0;index<headerNum;index++)
	{

		strcpy(headerList[index]->name, pro_ptr->pro_header_list[index].headerName);
		headerList[index]->num_fields=pro_ptr->pro_header_list[index].fieldNum;
		headerList[index]->sel_idx=pro_ptr->pro_header_list[index].selectFieldId;
		headerList[index]->sum_idx=pro_ptr->pro_header_list[index].checksumField;
		headerList[index]->num_next=pro_ptr->pro_header_list[index].selectNum;
		//set the fieldList
		for(field_index=0;field_index<headerList[index]->num_fields;field_index++)
		{
			strcpy(headerList[index]->fields[field_index].name,pro_ptr->pro_header_list[index].fieldList[field_index].fieldName);
			headerList[index]->fields[field_index].length=pro_ptr->pro_header_list[index].fieldList[field_index].length;
			headerList[index]->fields[field_index].offset=pro_ptr->pro_header_list[index].fieldList[field_index].offset;
			int isMatch=pro_ptr->pro_header_list[index].fieldList[field_index].isMatch;
			char name[32]="__";
			if(!isMatch)
			{
				strcat(name,headerList[index]->fields[field_index].name);
				strcpy(headerList[index]->fields[field_index].name,name);
			}

		}
		//set selectList
		for(next_index=0;next_index<headerList[index]->num_next;next_index++)
		{
			headerList[index]->next[next_index].h=headerList[pro_ptr->pro_header_list[index].selectList[next_index].headerId];
			value_t value;
			strcpy(value.v,pro_ptr->pro_header_list[index].selectList[next_index].value);
			headerList[index]->next[next_index].v=value;
			//the value is too large
		}
		//set the expression length
		struct expr* length=malloc(sizeof *length);
		length->type=pro_ptr->pro_header_list[index].expr_length.type;
		uint8_t left_fieldId=pro_ptr->pro_header_list[index].expr_length.leftFieldId;
		uint8_t right_fieldId=pro_ptr->pro_header_list[index].expr_length.rightFieldId;
		uint32_t left_Value=pro_ptr->pro_header_list[index].expr_length.leftVlaue;
		uint32_t right_Value=pro_ptr->pro_header_list[index].expr_length.rightValue;
		if(length->type==EXPR_FIELD)
		{
			if(left_fieldId==-1)
			{
				if(right_fieldId!=-1)
					strcpy(length->u.field,pro_ptr->pro_header_list[index].fieldList[right_fieldId].fieldName);
				else
					printf("left_fieldId and right_fieldId all are -1\n");
			}
			else
			{
				if(right_fieldId!=-1)
					printf("left_fieldId and right_fieldId all are not -1\n");
				else
					strcpy(length->u.field,pro_ptr->pro_header_list[index].fieldList[left_fieldId].fieldName);
			}
		}
		else if(length->type==EXPR_VALUE)
		{
			if(left_Value==-1)
			{
				if(right_Value!=-1)
				    length->u.value=right_Value;
				else
					printf("left_Value and right_Value all are -1\n");
			}
			else
			{
				if(right_Value!=-1)
					printf("left_Value and right_Value all are not -1\n");
				else
					length->u.value=left_Value;
			}
		}
		else if(length->type==EXPR_NOT)
		{
			length->u.sub_expr->type=EXPR_FIELD;
			if(left_fieldId==-1)
			{
				if(right_fieldId!=-1)
					strcpy(length->u.sub_expr->u.field,pro_ptr->pro_header_list[index].fieldList[right_fieldId].fieldName);
				else
					printf("left_fieldId and right_fieldId all are -1\n");
			}
			else
			{
				if(right_fieldId!=-1)
					printf("left_fieldId and right_fieldId all are not -1\n");
				else
					strcpy(length->u.sub_expr->u.field,pro_ptr->pro_header_list[index].fieldList[left_fieldId].fieldName);
			}
		}
		else if((length->type==EXPR_SUB)||(length->type==EXPR_AND)|| \
				(length->type==EXPR_OR)||(length->type==EXPR_XOR)||  \
				(length->type==EXPR_SHL)||(length->type==EXPR_ADD))
		{
			struct expr* left_expr=malloc(sizeof *left_expr);
			struct expr* right_expr=malloc(sizeof *right_expr);
			//left_expr
			if(left_fieldId==-1)
			{
				if(left_Value!=-1)
				{
					left_expr->type=EXPR_VALUE;
					left_expr->u.value=left_Value;
				}
				else
					printf("left_fieldId and left_Value all are -1\n");
			}
			else
			{
				left_expr->type=EXPR_FIELD;
				strcpy(left_expr->u.field,pro_ptr->pro_header_list[index].fieldList[left_fieldId].fieldName);
			}
			//right_expr
			if(right_fieldId==-1)
			{
				if(right_Value!=-1)
				{
					right_expr->type=EXPR_VALUE;
					right_expr->u.value=right_Value;
				}
				else
					printf("right_fieldId and right_Value all are -1\n");
			}
			else
			{
				right_expr->type=EXPR_FIELD;
				strcpy(right_expr->u.field,pro_ptr->pro_header_list[index].fieldList[right_fieldId].fieldName);
			}
		  //set the length
		   length->u.binop.left=left_expr;
		   length->u.binop.right=right_expr;
		}
		headerList[index]->length=length;

	}

	return ret;

}

static struct trace_tree *trace_tree_E(void)
{
	struct trace_tree_E *t = malloc(sizeof *t);
	t->h.type = TT_E;
	t->h.flag_new=1;
	return (struct trace_tree *)t;
}

static struct trace_tree *trace_tree_L(struct action *a)
{
	struct trace_tree_L *t = malloc(sizeof *t);
	t->h.type = TT_L;
	t->h.flag_new=1;
	t->ac = action_copy(a);
	t->index = -1;
	return (struct trace_tree *)t;
}

/*
 * author:milktank
 * time:2016-03-04
 * trace_tree_WM  func
 */
static struct trace_tree *trace_tree_WM(const char *name,
			int offset,
			int length,
			struct header *spec,
			struct trace_tree *tt)
{
	struct trace_tree_WM *t =malloc(sizeof *t);
	t->h.type= TT_WM;
	t->h.flag_new=1;
	strncpy(t->name, name, 32);
	t->name[31] = 0;
	t->offset=offset;
	t->length=length;
	t->spec=spec;
	t->t=tt;
	return  (struct trace_tree *)t;
}

static struct trace_tree *trace_tree_RM(const char *name,
				       value_t v,
				       struct trace_tree *p)
{
	struct trace_tree_RM *t = malloc(sizeof *t + sizeof (struct trace_tree_Vb));
	t->h.type = TT_RM;
	t->h.flag_new=1;
	strncpy(t->name, name, 32);
	t->name[31] = 0;
	t->num_branches = 1;
	t->branches[0].value = v;
	t->branches[0].tree = p;
	return (struct trace_tree *)t;
}


//Curtis
static struct trace_tree *trace_tree_WMU(const char *name,
			int length,
			value_t value,
			struct header *spec,
			struct trace_tree *tt)
{
	struct trace_tree_WMU *t =malloc(sizeof *t);
	t->h.type= TT_WMU;
	t->h.flag_new=1;
	strncpy(t->name, name, 32);
	t->name[31] = 0;
	//t->offset=offset;
	t->length=length;
	t->value=value;
	t->spec=spec;
	t->t=tt;
	return  (struct trace_tree *)t;
}

static struct trace_tree *trace_tree_RMU(const char *name,
				       value_t v,
				       struct trace_tree *p)
{
	struct trace_tree_RMU *t = malloc(sizeof *t + sizeof (struct trace_tree_Vb));
	t->h.type = TT_RMU;
	t->h.flag_new=1;
	strncpy(t->name, name, 32);
	t->name[31] = 0;
	t->num_branches = 1;
	t->branches[0].value = v;
	t->branches[0].tree = p;
	return (struct trace_tree *)t;
}

static struct trace_tree *trace_tree_V(const char *name,
				       value_t v,
				       struct trace_tree *p)
{
	struct trace_tree_V *t = malloc(sizeof *t + sizeof (struct trace_tree_Vb));
	t->h.type = TT_V;
	t->h.flag_new=1;
	strncpy(t->name, name, 32);
	t->name[31] = 0;
	t->num_branches = 1;
	t->branches[0].value = v;
	t->branches[0].tree = p;
	return (struct trace_tree *)t;
}

static struct trace_tree *trace_tree_T(const char *name,
				       value_t v,
				       struct trace_tree *tt,
				       struct trace_tree *tf)
{
	struct trace_tree_T *t = malloc(sizeof *t);
	t->h.type = TT_T;
	t->h.flag_new=1;
	strncpy(t->name, name, 32);
	t->name[31] = 0;
	t->value = v;
	t->t = tt;
	t->f = tf;
	t->barrier_index = -1;
	return (struct trace_tree *)t;
}

/*static struct trace_tree * trace_tree_D(const char *name,
				       const void *arg,
				       struct trace_tree *tt)
{
	struct trace_tree_D *t = malloc(sizeof *t);
	t->h.type = TT_D;
	t->h.flag_new=1;
	strncpy(t->name, name, 32);
	t->name[31] = 0;
	t->arg = arg;
	t->t = tt;
	return (struct trace_tree *)t;
}*/


static struct trace_tree *trace_tree_G(struct header *old_spec,
				       struct header *new_spec,
				       int stack_base,
				       struct trace_tree *tt)
{
	struct trace_tree_G *t = malloc(sizeof *t);
	t->h.type = TT_G;
	t->h.flag_new=1;
	t->ft = NULL;
	t->old_spec = old_spec;
	t->new_spec = new_spec;
	t->stack_base = stack_base;
	t->t = tt;
	t->index = -1;
	return (struct trace_tree *)t;
}

struct trace_tree *trace_tree(void)
{
	return trace_tree_E();
}
struct action *action_p(void)
{
	struct action *a = malloc(sizeof(struct action));
	a->num_actions = 0;
	return a;
}
struct action *action_copy(struct action *a)
{
	struct action *aa = action_p();
	memcpy(aa, a, sizeof(struct action));
	return aa;
}
void get_cur_header(struct packet_parser* pp,struct header* cur_spec)
{
	cur_spec=STACK_TOP(pp).spec;
}
static uint32_t packet_parser_read_to_32(struct packet_parser *pp, const char *field)
{
	int offset, length;
	value_t v;
	struct header *cur_spec = STACK_TOP(pp).spec;

	header_get_field(cur_spec, field, &offset, &length);
	assert((offset + length + 7) / 8 <= STACK_TOP(pp).length);
	v = value_extract(STACK_TOP(pp).data, offset, length);
	if(length < 8)
		return value_bits_to_8(length, v);
	else if (length == 8)
		return value_to_8(v);
	else if(length == 16)
		return value_to_16(v);
	else if(length == 32)
		return value_to_32(v);
	assert(0);
	return RET_OK;
}
static uint32_t expr_interp_fields(struct expr *e, struct packet_parser *pp,char *length_field_string, uint32_t * length_value)
{
	switch(e->type) {
	case EXPR_FIELD:
	{
		uint32_t a=packet_parser_read_to_32(pp, e->u.field);
		*length_value=a;
		strcpy(length_field_string,e->u.field);
//		printf("Find fields: %s\t%s\t value is %d\n",length_field_string,e->u.field,a);
		return a;
	}
	case EXPR_VALUE:
		return e->u.value;
	case EXPR_NOT:
		return ~ expr_interp_fields(e->u.sub_expr, pp,length_field_string,length_value);
	case EXPR_ADD:
		return expr_interp_fields(e->u.binop.left, pp,length_field_string,length_value) + expr_interp_fields(e->u.binop.right, pp,length_field_string,length_value);
	case EXPR_SUB:
		return expr_interp_fields(e->u.binop.left, pp,length_field_string,length_value) - expr_interp_fields(e->u.binop.right, pp,length_field_string,length_value);
	case EXPR_SHL:
		return expr_interp_fields(e->u.binop.left, pp,length_field_string,length_value) << expr_interp_fields(e->u.binop.right, pp,length_field_string,length_value);
	case EXPR_SHR:
		return expr_interp_fields(e->u.binop.left, pp,length_field_string,length_value) >> expr_interp_fields(e->u.binop.right, pp,length_field_string,length_value);
	case EXPR_AND:
		return expr_interp_fields(e->u.binop.left, pp,length_field_string,length_value) & expr_interp_fields(e->u.binop.right, pp,length_field_string,length_value);
	case EXPR_OR:
		return expr_interp_fields(e->u.binop.left, pp,length_field_string,length_value) | expr_interp_fields(e->u.binop.right, pp,length_field_string,length_value);
	case EXPR_XOR:
		return expr_interp_fields(e->u.binop.left, pp,length_field_string,length_value) ^ expr_interp_fields(e->u.binop.right, pp,length_field_string,length_value);
	}
	return 0;
}
struct trace_tree* pull_from_core(struct packet_parser *pp,struct header* cur_spec,struct trace_tree* root)
{
	value_t sel_value;
	uint8_t *cur_data;
	int cur_length;
	struct header *old_spec;
	int next_stack_top;
	/* XXX: hack */
	char length_field[32]={};
	uint32_t length_value=0;
	cur_spec = STACK_TOP(pp).spec;
	cur_data = STACK_TOP(pp).data;
	cur_length = STACK_TOP(pp).length;
	int i = cur_spec->sel_idx;
	assert(i >= 0);
	int j;
	value_t v = value_extract(cur_data,cur_spec->fields[i].offset,cur_spec->fields[i].length);
	old_spec = cur_spec;
	sel_value = v;
	//printf("v is:%d\n",value_to_16(v));
	for(j = 0; j < STACK_TOP(pp).spec->num_next; j++) {
			if(value_equal(cur_spec->next[j].v, v)) {
				int offset = expr_interp_fields(cur_spec->length, pp,length_field,&length_value);
				cur_spec->header_length=offset;
				if (length_value!=0)
				{

					value_t v = packet_parser_read(pp, length_field);

					//most import move.Add lengthfield to the trace_R().Some 'declare' to be add.
					//calculate length,we need to read the field
					root=trace_tree_V(length_field,v, root);
				}
				pp->stack_top++;
				next_stack_top = pp->stack_top;
				assert(pp->stack_top < 32);
				STACK_TOP(pp).data = cur_data + offset;
				STACK_TOP(pp).spec = cur_spec->next[j].h;
				STACK_TOP(pp).length = cur_length - offset;
				break;
			}
		}

	assert(STACK_TOP(pp).length >= 0);
	cur_spec = STACK_TOP(pp).spec;
	//pull_header we need to read the select_id value
	root=trace_tree_V(header_get_sel(old_spec), sel_value, root);
    //recode the trace_G node
	root=trace_tree_G(old_spec, cur_spec,next_stack_top, root);
	return root;
}
const char *header_get_sel(struct header *h)
{
	assert(h->sel_idx >= 0);
	return h->fields[h->sel_idx].name;
}
value_t packet_parser_read(struct packet_parser *pp, const char *field)
{
	int offset, length;
	struct header *cur_spec = STACK_TOP(pp).spec;
	header_get_field(cur_spec, field, &offset, &length);
	assert((offset + length + 7) / 8 <= STACK_TOP(pp).length);
	return value_extract(STACK_TOP(pp).data, offset, length);
}
void header_get_field(struct header *h, const char *name, int *offset, int *length)
{
	int i;
	for(i = 0; i < h->num_fields; i++)
		if(strcmp(name, h->fields[i].name) == 0) {
			*offset = h->fields[i].offset;
			*length = h->fields[i].length;
			return;
		}
	//xinfo("set the offset & length of metadata.");
	*offset = 48;
	*length = 48;
	return;
	//assert(0);
}

void push_header(struct packet *pkt)
{
	struct header *new_spec;
	int prev_stack_top;
	packet_parser_push(pkt->pp, &new_spec, &prev_stack_top);
	//sstrace_P(new_spec, prev_stack_top);
}
void packet_parser_push(struct packet_parser *pp, struct header **new_spec,
			int *prev_stack_top)
{
	assert(pp->stack_top > 0);
	*prev_stack_top = pp->stack_top;
	pp->stack_top--;
	*new_spec = STACK_TOP(pp).spec;
}

struct trace_tree* transfer_trace_from_core(struct packet* pkt,Trace* trace_ptr)
{
      int nodeNum=trace_ptr->num_trace_node;
      int index;
      struct trace_tree *root;
      ///get the current spec;
      struct header * cur_spec=malloc(sizeof(struct header));
      get_cur_header(pkt->pp,cur_spec);
      int pull_header_sum=0;
      for(index=nodeNum-1;index<nodeNum;index++)
      {
    	  switch(trace_ptr->t[index].type)
    	  {
    	  case TT_L:
    		  root =trace_tree_L(trace_ptr->t[index].u.tt_l.ac);
    		  break;
    	  case TT_V:
    		  root = trace_tree_V(trace_ptr->t[index].u.tt_v.name,trace_ptr->t[index].u.tt_v.value,root);
    		  break;
    	  case TT_T:
    		  if(trace_ptr->t[index].u.tt_t.flag)
    		  		root = trace_tree_T(trace_ptr->t[index].u.tt_t.name,
    		  							trace_ptr->t[index].u.tt_t.value,
    		  						    root,
    		  						    trace_tree_E());
    		  else
    			  root = trace_tree_T(trace_ptr->t[index].u.tt_t.name,
    			      		  							trace_ptr->t[index].u.tt_t.value,
    			      		  						    trace_tree_E(),
														root);
    		  break;
    	  case TT_G:
    		  //pull()produce the new_spewc,old_spec, statck-base.
    		  pull_header_sum++;
    		  root = pull_from_core(pkt->pp,cur_spec,root);
    		  break;
    	  case TT_WM:
    		  //get the current_spec;
    		  root=trace_tree_WM(trace_ptr->t[index].u.tt_wm.name,trace_ptr->t[index].u.tt_wm.offset,trace_ptr->t[index].u.tt_wm.length,cur_spec,root);
    		  break;
    	  case TT_RM:
    		  root=trace_tree_RM(trace_ptr->t[index].u.tt_rm.name,trace_ptr->t[index].u.tt_rm.value,root);
    		  break;
    	  case TT_WMU:
    		  root=trace_tree_WMU(trace_ptr->t[index].u.tt_wmu.name,trace_ptr->t[index].u.tt_wmu.length,trace_ptr->t[index].u.tt_wmu.value,cur_spec,root);
    		  break;
    	  case TT_RMU:
    		  root=trace_tree_RMU(trace_ptr->t[index].u.tt_rmu.name,trace_ptr->t[index].u.tt_rmu.value,root);
    		  break;
    	  default:
    		  break;

    	  }
      }
      for(index=0;index<pull_header_sum;index++)
      {
    	  push_header(pkt);
      }

      return root;

}




