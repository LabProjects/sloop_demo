/*
 * transfer.h
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "sloop/include/global_var.h"
#include "sloop/include/trace_tree.h"
#ifndef POP_METADATA_SLOOP_SOURCE_TRANSFER_H_
#define POP_METADATA_SLOOP_SOURCE_TRANSFER_H_

uint32_t transfer_Protocol_from_core_to_native(struct Set_Protocol* pro_ptr);
struct trace_tree* transfer_trace_from_core(struct packet* pkt,Trace* trace_ptr);
void get_cur_header(struct packet_parser* pp,struct header* cur_spec);
struct trace_tree* pull_from_core(struct packet_parser *pp,struct header* cur_spec,struct trace_tree* root);
const char *header_get_sel(struct header *h);
value_t packet_parser_read(struct packet_parser *pp, const char *field);
void header_get_field(struct header *h, const char *name, int *offset, int *length);
struct trace_tree* transfer_trace_from_core(struct packet*pkt,Trace* trace_ptr);
void push_header(struct packet*);
void packet_parser_push(struct packet_parser *pp, struct header **new_spec,int *prev_stack_top);

#endif /* POP_METADATA_SLOOP_SOURCE_TRANSFER_H_ */
