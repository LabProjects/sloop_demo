/*
 * up_report.c
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
//hosts_Report.c
//link_Report.c
//lldp_Report.c
//switch_Report.c
#include "global_var.h"
#include "param.h"
#include "msg_header.h"
#include "process_msg.h"
#include "up_report.h"
#include "pof_byte_transfer.h"
extern uint32_t g_upward_xid;

uint32_t hostReport(uint8_t reason,uint32_t dpid,uint32_t portId,uint32_t hostId,uint32_t hostIp)
{

	struct HostReport hostreport;
	hostreport.attachedSwitchId=dpid;
	hostreport.attachedPortId=portId;
	hostreport.hostId=hostId;
	hostreport.hostIp=hostIp;
	hostreport.reason=reason;
	pof_HtoN_transfer_hostReport(&hostreport);
	if(RET_OK!=reply_msg(HOSTS_REPORT,g_upward_xid++,sizeof(struct HostReport), (uint8_t *)&hostreport))
	{
          printf("Host Report is Failed!\n");
	}

	uint32_t ok=RET_OK;

	return ok;
}
uint32_t switchReport(uint8_t reason,uint32_t dpid,uint8_t portNum,uint8_t* portReasons,struct PortReport * port)
{
    int i=0;
	struct SwitchReport switchreport;
	switchreport.switchReason=reason;
	switchreport.switchId=dpid;
	switchreport.port_Num=portNum;
	for(i=0;i<OFP_MAX_PORTS_NUM_PER_REPORT;i++)
	{
		switchreport.portReasons[i]=portReasons[i];
		//copy port
		switchreport.port[i].Invalid_padd=0;
		memset(switchreport.port[i].Invalid_padd_1,0,sizeof(uint8_t)*7);
		switchreport.port[i].advertisedFeatures=port->advertisedFeatures;
		switchreport.port[i].config=port->config;
		switchreport.port[i].currentFeatures=port->currentFeatures;
		switchreport.port[i].currentSpeed=port->currentSpeed;
		switchreport.port[i].deviceId=port->deviceId;
		switchreport.port[i].maxSpeed=port->maxSpeed;
		switchreport.port[i].openflowEnable=port->openflowEnable;
		switchreport.port[i].peerFeatures=port->peerFeatures;
		switchreport.port[i].portId=port->portId;
		switchreport.port[i].state=port->state;
		switchreport.port[i].supportedFeatures=port->supportedFeatures;
		memcpy(switchreport.port[i].hwa,port->hwa,sizeof(uint8_t)*7);
		memcpy(switchreport.port[i].name,port->name,sizeof(uint8_t)*64);
	}
	switchreport.Invalid_padd=0;
	memset(switchreport.Invalid_padd1,0,sizeof(uint8_t)*30);
	pof_HtoN_transfer_switchReport(&switchreport);
	if(RET_OK!=reply_msg(HOSTS_REPORT,g_upward_xid++,sizeof(struct HostReport), (uint8_t *)&switchreport))
	{
          printf("Host Report is Failed!\n");
	}

	uint32_t ok=RET_OK;

	return ok;
}
uint32_t linkReport(uint32_t dstPort,uint32_t dstSwitch,uint32_t srcPort,uint32_t srcSwitch,uint8_t linkstate,uint8_t linktype,uint8_t reason)
{
	int i=0;
	struct LinkReport linkreport;
	memset(linkreport.Invalid_padd,0,sizeof(uint8_t)*5);
	linkreport.dstPortId=dstPort;
	linkreport.dstSwitchId=dstSwitch;
	linkreport.linkstate=linkstate;
	linkreport.linktype=linktype;
	linkreport.reason=reason;
	linkreport.srcPortId=srcPort;
	linkreport.srcSwitchId=srcSwitch;
	pof_HtoN_transfer_linkReport(&linkreport);
	if(RET_OK!=reply_msg(LINK_REPORT,g_upward_xid++,sizeof(struct LinkReport), (uint8_t *)&linkreport))
		{
	          printf("Host Report is Failed!\n");
		}
	uint32_t ok=RET_OK;
	return ok;
}

//???????????????????????????
uint32_t lldpReport()
{
	//struct LldpReport


	return RET_OK;
}
