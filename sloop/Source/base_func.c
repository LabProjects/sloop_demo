/*
 * base_func.c
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "../../sloop/Source/base_func.h"

#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <net/if.h>
#include <sys/ioctl.h>
#include <linux/sockios.h>
#include <ifaddrs.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <time.h>
#include <signal.h>

#include "sloop/include/global_var.h"
#include "sloop/include/param.h"
#include "sloop/Source/msg.h"
#include "sloop/Source/xlog.h"
extern conn_desc core_conn_desc;
extern uint32_t cluster_id;
extern uint64_t deviceFP;
extern uint32_t g_upward_xid;
extern uint32_t send_queue_id;
uint32_t build_header(struct core_header *header,uint8_t type, uint16_t len, uint32_t xid)
{
    header->version = CORE_VERSION;
    header->type = type;
    header->length = len;
    header->xid = xid;
	pof_HtoN_transfer_header(header);

    return RET_OK;
}

uint32_t connect_config( char *core_ip, uint16_t core_port,uint32_t retry_max,uint32_t retry_interval,uint32_t echo_interval)
{
			memset((void *)&core_conn_desc, 0, sizeof(conn_desc)); //clean the core_conn_desc
		    memcpy((void*)core_conn_desc.core_ip, (void*)core_ip, strlen(core_ip)); //set the core_ip
		    core_conn_desc.core_port = core_port; //set the core_port;
            core_conn_desc.retry_max=retry_max;
            core_conn_desc.retry_interval=retry_interval;
            core_conn_desc.echo_interval=echo_interval;
            xinfo("******core_port:%d********\n",core_conn_desc.core_port);
            xinfo("******retry_max:%d********\n",core_conn_desc.retry_max);
            xinfo("******retry_interval:%d********\n",core_conn_desc.retry_interval);
            xinfo("******echo_interval:%d********\n",core_conn_desc.echo_interval);
            //init deviceFP
            produce_deviceFP();
		    return RET_OK;
}
uint32_t get_hwaddr_by_name(const char *name,unsigned char *hwaddr)
{
    struct ifreq ifr; /* Interface request. */
    struct sockaddr_in *sin;
    int    sock;

    if(-1 == (sock = socket(AF_INET, SOCK_STREAM, 0))){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_CREATE_SOCKET_FAILURE);
    }

    memset(hwaddr, 0, CORE_ETH_ALEN);
    memset(&ifr, 0, sizeof(ifr));
    strncpy(ifr.ifr_name, name, strlen(name)+1);

    /* Get hardware address. */
    if(-1 == ioctl(sock, SIOCGIFHWADDR, &ifr)){
        close(sock);
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_GET_PORT_INFO_FAILURE);
    }else{
        memcpy(hwaddr, ifr.ifr_hwaddr.sa_data, CORE_ETH_ALEN);
    }


    close(sock);
    return RET_OK;
}
uint32_t get_name_by_ip(char* name,char* ip)
{
    struct ifaddrs *addrs, *tmp;
    struct sockaddr_in *sa;
    char localIP[CORE_IP_LEN]="\0";
    getifaddrs(&addrs);
    for(tmp=addrs; tmp!=NULL; tmp=tmp->ifa_next){
        if(tmp->ifa_addr && tmp->ifa_addr->sa_family == AF_PACKET){
        	sa=(struct sockaddr_in *)tmp->ifa_addr;
        	strcpy(localIP,inet_ntoa(sa->sin_addr));
        	if(strcmp(localIP,ip))
        	{
        		name=tmp->ifa_name;
        	}
        }
    }
    freeifaddrs(addrs);
    return RET_OK;
}
uint32_t connect_core(int socket_fd, char *server_ip, uint16_t port){
		//conn_desc *conn_desc_ptr = (conn_desc *)&core_conn_desc;
	    socklen_t sockaddr_len = sizeof(struct sockaddr_in);
	    struct sockaddr_in serverAddr, localAddr;
	    char localIP[CORE_IP_LEN] = "\0";
	    uint8_t hwaddr[CORE_ETH_ALEN] = {0};


	    /* Build server socket address. */
	    memset ((char *) &serverAddr, 0,  sizeof (struct sockaddr_in));
	    serverAddr.sin_family = AF_INET;
	    serverAddr.sin_port = htons(port);
	    serverAddr.sin_addr.s_addr = inet_addr(server_ip);

	    if(connect(socket_fd, (struct sockaddr *)&serverAddr, sizeof (struct sockaddr_in)) == -1){
	        close(socket_fd);
	        return (CONNECT_SERVER_FAILURE);
	    }

	    /////////////////////produce the cluster_id;
	    if(cluster_id == 0){
	        if(getsockname(socket_fd, (struct sockaddr *)&localAddr, &sockaddr_len) != RET_OK){
	            //POF_ERROR_CPRINT_FL("Get socket name fail!");
	            close(socket_fd);
	            return ERROR;
	        }
	        strcpy(localIP, inet_ntoa(localAddr.sin_addr));
	        char name[PORT_NAME]="\0";
	        get_name_by_ip(name,localIP);
	        get_hwaddr_by_name(name,hwaddr);
	        memcpy(&cluster_id, hwaddr+2, CORE_ETH_ALEN-2);
	        POF_NTOHL_FUNC(cluster_id);

	        if(cluster_id == 0){
	            cluster_id = 1;
	        }
	    }
	    //from cluster_id produce the deviceFP
	    produce_deviceFP();


	    return RET_OK;
}

void produce_deviceFP()
{
      deviceFP=cluster_id;
      deviceFP=deviceFP << 32;
      deviceFP |=cluster_id;
}

uint32_t core_create_socket(int *socket_fd_ptr){
    /* Socket file descriptor. */
    int socket_fd;

    if ((socket_fd = socket(AF_INET, SOCK_STREAM, 0)) == -1){
       printf("Create socket failure!\n");
        return (CREATE_SOCKET_FAILURE);
    }
    *socket_fd_ptr = socket_fd;

    return RET_OK;
}
uint32_t echo_timer_create(uint32_t delay,uint32_t interval, TIMER_FUNC timer_handler,uint32_t *timer_id_ptr)
{
    struct itimerval val;

    if(timer_handler == NULL || timer_id_ptr == NULL){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TIMER_CREATE_FAIL);
    	printf("timer create failure\n");
    }

    /* Set the value and the interval of the timer. */
    val.it_interval.tv_sec = (uint32_t)(interval / 1000);
    val.it_interval.tv_usec = (interval % 1000) * 1000;
    val.it_value.tv_sec = (uint32_t)(delay / 1000);
    val.it_value.tv_usec = (delay % 1000) * 1000;

    /* Set the timer function. */
    if(SIG_ERR == signal(SIGALRM, (__sighandler_t)timer_handler)){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TIMER_CREATE_FAIL);
    	printf("timer handle set failure\n");
    }

    /* Set the timer with val. */
    if(RET_OK != setitimer(ITIMER_REAL, &val, NULL)){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TIMER_CREATE_FAIL);
    	printf("timer handle set failure\n");
    }

    *timer_id_ptr = VALID_TIMERID;
    xinfo("**********TIMEID:%d*******\n",*timer_id_ptr);
    return RET_OK;
}

uint32_t echo_timer_delete(uint32_t *timer_id_ptr){
    uint32_t timer_id = INVALID_TIMERID;

    if(NULL == timer_id_ptr){
       // POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TIMER_DELETE_FAIL);
    }
    timer_id = *timer_id_ptr;

    if(timer_id == INVALID_TIMERID){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TIMER_DELETE_FAIL);
    }

    /* Delete the timer. */
    if(RET_OK != setitimer(ITIMER_REAL, NULL, NULL)){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TIMER_DELETE_FAIL);
    }

    /* Reset the signal function. */
    if(SIG_ERR == signal(SIGALRM, NULL)){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_TIMER_DELETE_FAIL);
    }

    *timer_id_ptr = INVALID_TIMERID;

    return RET_OK;
}

uint32_t echo_timer(uint32_t timer_id, int arg){
    conn_desc *conn_desc_ptr = (conn_desc *)&core_conn_desc;
    struct core_header head;
    uint16_t len;
    uint32_t ret;

    /* If valid, fetch one message and send it to controller. */
    switch(conn_desc_ptr->conn_status){
        case CHANNEL_INVALID:
        case CHANNEL_CONNECTING:
        case CHANNEL_CONNECTED:
        case WAIT_HELLO:
        case FEATURE_REQUEST:
        case WAIT_SET_PROTO:
        case WAIT_SET_FUN:
            break;
        case CHANNEL_RUN:
            /* Send echo to controller. */
            /* Build echo message. */
            len = sizeof(struct core_header);
            build_header(&head, ECHO_REQUEST, len, g_upward_xid++);

            /* Write error message into queue for sending. */
            ret = msg_queue_write(send_queue_id, (char*)&head, len, WAIT_FOREVER);
            if(ret != RET_OK){
                //pofsc_set_error(POFET_SOFTWARE_FAILED, ret);
                printf("ECHO_REQUEST FAILURE\n");
            }
            break;
        default:
            break;
    }
    return RET_OK;
}


