/*
 * core_pnpl.c
 *
 *  Created on: Aug 2, 2018
 *      Author: sloop
 */
#include "../../sloop/Source/start.h"

#include <signal.h>

#include "sloop/include/global_var.h"
#include "sloop/include/param.h"
#include "sloop/Source/base_func.h"
#include "sloop/Source/msg.h"
#include "sloop/Source/task.h"
#include "sloop/Source/xlog.h"
task_t main_task_id = 0;
task_t send_msg_task_id = 0;
char queue_msg_buf[MESSAGE_MAX_SIZE] = {0};
char *DEFAULT_CORE_IP="127.0.0.1";
uint32_t DEFAULT_CORE_PORT=9000;
uint32_t conn_max_retry = CONNECTION_MAX_RETRY_TIME;

uint32_t conn_retry_interval = CONNECTION_RETRY_INTERVAL;

conn_desc core_conn_desc;
uint32_t send_queue_id = INVALID_QUEUEID;
uint32_t cluster_id=0;
uint64_t deviceFP=0;
key_t queue_key = 0;
uint32_t g_upward_xid = CORE_INITIAL_XID;
uint32_t echo_interval = ECHO_INTERVAL;
uint32_t echo_timer_id = 0;
uint32_t policy_status=UNKNOWN;
uint32_t pro_status=UNKNOWN;
struct header* headerList[MAX_HEADER_SUM];
int start()
{
	     /* Set the signal handle function. */
	    //signal(SIGINT, terminate_handler);         //deal with the CTRL+C follow up...........
	    //signal(SIGTERM, terminate_handler);
	    signal(SIGPIPE, SIG_IGN);                    //deal with the socket close.............

	    /* Set CORE connection attributes. */
	    connect_config(DEFAULT_CORE_IP,DEFAULT_CORE_PORT,conn_max_retry,conn_retry_interval,echo_interval);
        xinfo("*********************Start*********************\n");
	    /* Create one message queue for storing messages to be sent to core. */
        xinfo("***********Create msg_queue*********************\n");
	    if (RET_OK != msg_queue_create(&(send_queue_id))){
	        printf("Create message queue, fail and return!\n");
	        return QUEUE_ERROR;
	    }

	    /* Create connection and state machine task. */
	   /* xinfo("***********Create test_task Thread*********************\n");
	    if (RET_OK != task_create(NULL, (void *)test_task, &main_task_id)){
	   	        printf("\nCreate openflow main task, fail and return!");
	   	        return ERROR;
	   	    }*/
	    xinfo("***********Create main_task Thread*********************\n");
	    if (RET_OK != task_create(NULL, (void *)main_task, &main_task_id)){
	        printf("\nCreate openflow main task, fail and return!");
	        return ERROR;
	    }
	    xinfo("******main_task thread ID:%d*********\n",main_task_id);
	    //POF_DEBUG_CPRINT_FL(1,GREEN,">>Startup openflow task!");

	    /* Create one task for sending  message to core asynchronously. */
	    xinfo("***********Create send_msg_task Thread*********************\n");
	    if (RET_OK != task_create(NULL, (void *)send_msg_task, &send_msg_task_id)){
	        //POF_ERROR_CPRINT_FL("\nCreate openflow main task, fail and return!");
	        return ERROR;
	    }
	    xinfo("******send_msg_task thread ID:%d*********\n",send_msg_task_id);
	    //POF_DEBUG_CPRINT_FL(1,GREEN,">>Startup task for sending message!");



	    /* Create one timer for sending echo message. */
	    xinfo("******Create the echo timer*********\n");
	    if (RET_OK != echo_timer_create( 1000, echo_interval, (void *)echo_timer, &echo_timer_id)){
	        //POF_ERROR_CPRINT_FL("\nCreate echo timer, fail and return!");
	        return ERROR;
	    }

	    return RET_OK;
}
uint32_t destroy()
{
    //uint32_t i;
	//uint16_t port_number = 0;
   // struct pof_local_resource *lr, *lrNext;

    /* Free task,timer and queue. */
    if(main_task_id != INVALID_TASKID){
        task_delete(&main_task_id);
    }

    if(send_msg_task_id != INVALID_TASKID){
        task_delete(&send_msg_task_id);
    }

    if(echo_timer_id != INVALID_TIMERID){
        echo_timer_delete(&echo_timer_id);
    }

    if(send_queue_id != INVALID_QUEUEID){
        msg_queue_delete(&send_queue_id);
    }

	//pof_close_log_file();

    return RET_OK;
}
