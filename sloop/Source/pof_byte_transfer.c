/**
 * Copyright (c) 2012, 2013, Huawei Technologies Co., Ltd.
 * All rights reserved.
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met: 
 * 
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer. 
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution. 
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "sloop/Source/pof_byte_transfer.h"

#include "sloop/include/action.h"
#include "sloop/include/global_var.h"
#include "sloop/include/msg_header.h"
#include "sloop/include/pof_common.h"
#include "sloop/include/trace_tree.h"
uint32_t pof_NtoH_transfer_header(void *ptr){

	struct core_header* head_p = (struct core_header*)ptr;
    POF_NTOHS_FUNC(head_p->length);
    POF_NTOHL_FUNC(head_p->xid);

    return RET_OK;
}

uint32_t pof_HtoN_transfer_header(void *ptr){
	struct core_header *head_p = (struct core_header *)ptr;

    POF_HTONS_FUNC(head_p->length);
    POF_HTONL_FUNC(head_p->xid);

    return RET_OK;
}
//deal with the trace from message
uint32_t pof_NtoH_transfer_trace(void*ptr)
{
   Trace* trace_ptr=(Trace*)ptr;
   POF_NTOHL_FUNC(trace_ptr->num_trace_node);
   int i=0;
   for(i=0;i<TRACE_NODE_MAX_NUM;i++)
   {
	   if(RET_OK!=trace_node_NtoH_transfer((trace_ptr->t +i)))
		   return ERROR;
   }
   return RET_OK;
}
uint32_t trace_node_NtoH_transfer(void*ptr)
{
	trace_node* trace_node_ptr=(trace_node*)ptr;
	POF_NTOHL_FUNC(trace_node_ptr->type);
	struct action* ac;
	tt_l* tt_l_ptr;
	//tt_v* tt_v_ptr;
	tt_t* tt_t_ptr;
	//tt_g* tt_g_ptr;
	tt_wm* tt_wm_ptr;
	//tt_rm* tt_rm_ptr;
	tt_wmu* tt_wmum_ptr;
	//tt_rmu* tt_rmu_ptr;
	int i=0;
	switch(trace_node_ptr->type)
	{
	/*case TT_E:
		tt_e* tt_e_ptr=(tt_e*)(trace_node_ptr->u);
		POF_NTOHL_FUNC(tt_e_ptr->next);
		break;*/
	case TT_L:
		tt_l_ptr=(tt_l*)(&(trace_node_ptr->u));
		//transfer action;
		ac=tt_l_ptr->ac;
		POF_NTOHL_FUNC(ac->num_actions);

		for(i=0;i<ac->num_actions;i++)
		{
			POF_NTOHL_FUNC(ac->a[i].type);
			switch(ac->a[i].type)
			{
			  case AC_CALC_R:
				  POF_NTOHL_FUNC(ac->a[i].u.op_r.dst_length);
				  POF_NTOHL_FUNC(ac->a[i].u.op_r.dst_offset);
				  POF_NTOHL_FUNC(ac->a[i].u.op_r.dst_type);
				  POF_NTOHL_FUNC(ac->a[i].u.op_r.op_type);
				  POF_NTOHL_FUNC(ac->a[i].u.op_r.src_length);
				  POF_NTOHL_FUNC(ac->a[i].u.op_r.src_offset);
				  POF_NTOHL_FUNC(ac->a[i].u.op_r.src_type);
				  break;
			  case AC_CALC_I:
				  POF_NTOHL_FUNC(ac->a[i].u.op_i.dst_length);
				  POF_NTOHL_FUNC(ac->a[i].u.op_i.dst_offset);
				  POF_NTOHL_FUNC(ac->a[i].u.op_i.dst_type);
				  POF_NTOHL_FUNC(ac->a[i].u.op_i.op_type);
				  POF_NTOHL_FUNC(ac->a[i].u.op_i.src_value);
				  break;
			  case AC_WRITE_METADATA:
				  POF_NTOHL_FUNC(ac->a[i].u.write_metadata.dst_length);
				  POF_NTOHL_FUNC(ac->a[i].u.write_metadata.dst_offset);
				  //value_t no need to transfer;
				  break;
			  case AC_WRITE_METADATA_FROM_PACKET:
				  POF_NTOHL_FUNC(ac->a[i].u.write_metadata_from_packet.dst_length);
				  POF_NTOHL_FUNC(ac->a[i].u.write_metadata_from_packet.dst_offset);
				  POF_NTOHL_FUNC(ac->a[i].u.write_metadata_from_packet.pkt_offset);
				  break;
			  case AC_MOVE_PACKET:
				  POF_NTOHL_FUNC(ac->a[i].u.move_packet.dir);
				  POF_NTOHL_FUNC(ac->a[i].u.move_packet.length);
				  POF_NTOHL_FUNC(ac->a[i].u.move_packet.offset);
				  POF_NTOHL_FUNC(ac->a[i].u.move_packet.type);
				  break;
			  case AC_MOVE_PACKET_IMM:
				  POF_NTOHL_FUNC(ac->a[i].u.move_packet_imm.dir);
				  POF_NTOHL_FUNC(ac->a[i].u.move_packet_imm.value);
				  break;
			  case AC_GOTO_TABLE:
				  POF_NTOHL_FUNC(ac->a[i].u.goto_table.offset);
				  POF_NTOHL_FUNC(ac->a[i].u.goto_table.tid);
				  break;
			  case AC_OUTPUT:
			  case AC_COUNTER:
				  POF_NTOHL_FUNC(ac->a[i].u.arg);
				  break;
			  case AC_SET_FIELD:
				  POF_NTOHL_FUNC(ac->a[i].u.set_field.dst_length);
				  POF_NTOHL_FUNC(ac->a[i].u.set_field.dst_offset);
				  break;
			  case AC_ADD_FIELD:
				  POF_NTOHL_FUNC(ac->a[i].u.add_field.dst_length);
				  POF_NTOHL_FUNC(ac->a[i].u.add_field.dst_offset);
				  break;
			  case AC_DEL_FIELD:
				  POF_NTOHL_FUNC(ac->a[i].u.del_field.dst_length);
				  POF_NTOHL_FUNC(ac->a[i].u.del_field.dst_offset);
				  break;
			  case AC_CHECKSUM:
				  POF_NTOHL_FUNC(ac->a[i].u.checksum.cal_length);
				  POF_NTOHL_FUNC(ac->a[i].u.checksum.cal_offset);
				  POF_NTOHL_FUNC(ac->a[i].u.checksum.sum_length);
				  POF_NTOHL_FUNC(ac->a[i].u.checksum.sum_offset);
				  break;
			  case AC_DROP:
			  case AC_PACKET_IN:
				  break;
			  default:
				  return ERROR;
				  break;
			 }
		  }
		break;
	case TT_V:
		//tt_v_ptr=(tt_v*)(trace_node_ptr->u);
		//name,value doesn't need to transfer    8-bit
		break;
	case TT_T:
		tt_t_ptr=(tt_t*)(&(trace_node_ptr->u));
		POF_NTOHL_FUNC(tt_t_ptr->flag);
		//name, value doesn't need to transfer 8-bit
		break;
	case TT_G:
		//tt_g_ptr=(tt_g*)(trace_node_ptr->u);
		break;
	case TT_WM:
		tt_wm_ptr=(tt_wm*)(&(trace_node_ptr->u));
		POF_NTOHL_FUNC(tt_wm_ptr->offset);
		POF_NTOHL_FUNC(tt_wm_ptr->length);
		break;
	case TT_RM:
		//tt_rm* tt_rm_ptr=(tt_rm*)(trace_node_ptr->u);
		break;
	case TT_WMU:
		tt_wmum_ptr=(tt_wmu*)(&(trace_node_ptr->u));
		POF_NTOHL_FUNC(tt_wmum_ptr->length);
		break;
	case TT_RMU:
		//tt_rmu* tt_rmu_ptr=(tt_rmu*)(trace_node_ptr->u);
		break;
	default:
		return ERROR;
		break;
	}
	return RET_OK;
}
uint32_t pof_HtoN_transfer_feature_reply(void *ptr)
{
	struct FeatureReply* feature_reply_ptr=(struct FeatureReply*)ptr;
	POF_HTONL_FUNC(feature_reply_ptr->nodeId);
	POF_HTONL_FUNC(feature_reply_ptr->nodeRole);
	POF_HTON64_FUNC(feature_reply_ptr->deviceFingerprint);
    return RET_OK;
}
uint32_t pof_HtoN_transfer_protocol_reply(void*ptr)
{
	struct ProtocolReply* pro_ptr=(struct ProtocolReply*)ptr;
	POF_HTONL_FUNC(pro_ptr->pro_status);
	POF_HTONL_FUNC(pro_ptr->Invalid_padd);
	return RET_OK;
}
uint32_t pof_HtoN_transfer_policy_reply(void *ptr)
{
	struct PolicyReply* pol_ptr=(struct PolicyReply*)ptr;
	POF_HTONL_FUNC(pol_ptr->policy_status);
	POF_HTONL_FUNC(pol_ptr->Invalid_padd);
	return RET_OK;
}
uint32_t pof_HtoN_transfer_FDT_reply(void* ptr)
{
  return RET_OK;
}
uint32_t pof_NtoH_trasfer_set_protocol(void*ptr)
{
	struct Set_Protocol* pro_ptr=(struct Set_Protocol*)ptr;
	//pro_ptr->protocolName don't need to transfer;
	POF_NTOHL_FUNC(pro_ptr->headerNum);
	int i=0;
	for(i=0;i<10;i++)
	{
		pof_NtoH_transfer_protocol_header(pro_ptr->pro_header_list+i);
	}
	return RET_OK;
}
uint32_t pof_NtoH_transfer_protocol_header(void* ptr)
{
	struct ProtocolHeader* pro_header_ptr=(struct ProtocolHeader*)ptr;
    //header_name don't need to transfer;
    int index=0;
    for(index=0;index<10;index++)
    {
    	POF_NTOHS_FUNC(pro_header_ptr->fieldList[index].fieldId);
    	POF_NTOHS_FUNC(pro_header_ptr->fieldList[index].length);
    	POF_NTOHS_FUNC(pro_header_ptr->fieldList[index].offset);
    }
   /* for(index=0;index<10;index++)
    {
    	POF_NTOHS_FUNC(pro_header_ptr->selectList[index].fieldId);
    	POF_NTOHS_FUNC(pro_header_ptr->selectList[index].length);
    	POF_NTOHS_FUNC(pro_header_ptr->selectList[index].offset);
    }*/
	return RET_OK;
}
uint32_t pof_HtoN_transfer_hostReport(void* ptr)
{

	struct HostReport* host_ptr=(struct HostReport*) ptr;
	POF_HTONL_FUNC(host_ptr->attachedPortId);
	POF_HTONL_FUNC(host_ptr->attachedSwitchId);
	POF_HTONL_FUNC(host_ptr->hostId);
	POF_HTONL_FUNC(host_ptr->hostIp);
	return RET_OK;
}
uint32_t pof_HtoN_transfer_switchReport(void* ptr)
{
	struct SwitchReport* switch_ptr=(struct SwitchReport*) ptr;
    POF_HTONL_FUNC(switch_ptr->switchId);
    int i=0;
    for(i=0;i<switch_ptr->port_Num;i++)
    {
    	POF_HTONL_FUNC(switch_ptr->port[i].advertisedFeatures);
    	POF_HTONL_FUNC(switch_ptr->port[i].config);
    	POF_HTONL_FUNC(switch_ptr->port[i].currentFeatures);
    	POF_HTONL_FUNC(switch_ptr->port[i].currentSpeed);
    	POF_HTONL_FUNC(switch_ptr->port[i].deviceId);
    	POF_HTONL_FUNC(switch_ptr->port[i].maxSpeed);
    	POF_HTONL_FUNC(switch_ptr->port[i].peerFeatures);
    	POF_HTONL_FUNC(switch_ptr->port[i].portId);
    	POF_HTONL_FUNC(switch_ptr->port[i].state);
    	POF_HTONL_FUNC(switch_ptr->port[i].supportedFeatures);
    }
   return RET_OK;
}
uint32_t pof_HtoN_transfer_linkReport(void* ptr)
{
            struct LinkReport* link_ptr=(struct LinkReport* )ptr;
            POF_HTONL_FUNC(link_ptr->dstPortId);
            POF_HTONL_FUNC(link_ptr->dstSwitchId);
            POF_HTONL_FUNC(link_ptr->srcPortId);
            POF_HTONL_FUNC(link_ptr->srcSwitchId);
            return RET_OK;

}
//uint32_t pof_HtoN_transfer_lldpReport(void*ptr);
