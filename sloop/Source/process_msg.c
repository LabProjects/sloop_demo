/*
 * run_process_message.c
 *
 *  Created on: Jul 31, 2018
 *      Author: sloop
 */
#include "sloop/Source/process_msg.h"

#include <assert.h>

#include "sloop/include/global_var.h"
#include "sloop/include/msg_header.h"
#include "sloop/include/param.h"
#include "sloop/include/spec_header.h"
#include "sloop/include/trace_tree.h"
#include "sloop/include/value.h"
#include "sloop/Source/msg.h"
#include "sloop/Source/pof_byte_transfer.h"
#include "sloop/Source/transfer.h"

extern char queue_msg_buf[MESSAGE_MAX_SIZE];
extern uint32_t policy_status;
extern uint32_t pro_status;
extern uint32_t send_queue_id;
extern conn_desc core_conn_desc;
extern uint32_t cluster_id;
extern uint64_t deviceFP;
uint32_t run_process_msg(char *message, uint16_t len){
    uint32_t ret = RET_OK;
    struct core_header *head_ptr;
    conn_desc *conn_desc_ptr = (conn_desc *)&core_conn_desc;

    head_ptr = (struct core_header *)message;
    if(POF_NTOHS(head_ptr->length)!= len){
        printf("packet length is wrong");
        return ERROR;
    }
    if(head_ptr->type == ECHO_REPLY){
           /* Record last echo time. */
           conn_desc_ptr->last_echo_time = time(NULL);
       }else{
           /* Forward to LPU board through IPC channel. */
    	   ret = parse_msg_from_core(message);
       }



    return ret;
}

uint32_t  parse_msg_from_core(char* msg_ptr)
{

	struct core_header       *header_ptr, head;
	struct  Set_Protocol      *pro_ptr;
    uint32_t          ret = RET_OK;
    uint16_t          len=0;
    uint8_t           msg_type;
    uint32_t          g_recv_xid;
    struct FeatureReply	  feature_reply;
    struct ProtocolReply     proReply;
    struct FDTReply          fdtReply;
    struct  PolicyReply       policyReply;
    header_ptr = (struct core_header*)msg_ptr;
    len = POF_NTOHS(header_ptr->length);

    /* Parse the Core packet header. */
    pof_NtoH_transfer_header(header_ptr);
    msg_type = header_ptr->type;
    g_recv_xid = header_ptr->xid;
    Trace* trace_ptr;
    /* Execute different responses according to the message type. */
    switch(msg_type){
        case ECHO_REQUEST:
            if(RET_OK != reply_msg(ECHO_REPLY, g_recv_xid, 0, NULL)){
                //POF_ERROR_HANDLE_RETURN_UPWARD(POFET_SOFTWARE_FAILED, POF_WRITE_MSG_QUEUE_FAILURE, g_recv_xid);
            }
            break;
        case FEATURE_REQUEST:
        	//uint32_t cluster_id;
        	//uint32_t ROLE;
        	//char deviceFingerPrint[64]
            feature_reply.nodeId=cluster_id;
            feature_reply.nodeRole=HP_SEC;
            feature_reply.deviceFingerprint=deviceFP;
            pof_HtoN_transfer_feature_reply(&feature_reply);
        	    if(RET_OK != reply_msg(FEATURE_REPLY, g_recv_xid, sizeof(struct FeatureReply), (uint8_t *)&feature_reply)){
        	        //POF_ERROR_HANDLE_RETURN_UPWARD(POFET_SOFTWARE_FAILED, POF_WRITE_MSG_QUEUE_FAILURE, g_recv_xid);
        	    }
            break;
        case SET_PROTOCOL:
        	/*receive the protocol*/
        	pro_ptr=(struct Set_Protocol*)(msg_ptr+sizeof(struct core_header));
        	pof_NtoH_trasfer_set_protocol(pro_ptr);
        	printf("protocol name:%s\n",pro_ptr->protocolName);
        	/*transfer the Protocol from the Core to Protocol_native*/
        	/*int index=0;
        	int headerNum=pro_ptr->headerNum;
        	header* headerArray[10];
        	for(index=0;index<headerNum;index++)
        	{
        		header* headerptr=malloc(sizeof(header));
        		headerptr->name=pro_ptr->pro_header_list[index].headerName;
        		headerptr->num_fields=pro_ptr->pro_header_list[index].fieldNum;
        		headerptr->fields[]
        		headerArray[index]=headerptr;
        	}*/
        	if(RET_OK!=transfer_Protocol_from_core_to_native(pro_ptr))
        	{

        	}


            /*reply the core the protocol status*/

        	proReply.pro_status=pro_status;
        	proReply.Invalid_padd=0;
        	pof_HtoN_transfer_protocol_reply(&proReply);
        	if(RET_OK!=reply_msg(GET_PROTOCOL_REPLY,g_recv_xid,sizeof(struct ProtocolReply),(uint8_t *)&proReply))
        	{
                     printf("reply protocol failure\n");
        	}
        	break;
        case SET_POLICY:
        	/*receive the protocol*/

        	/*reply the core the protocol status*/

        	policyReply.policy_status=policy_status;
        	policyReply.Invalid_padd=0;
        	pof_HtoN_transfer_policy_reply(&policyReply);
        	if(RET_OK!=reply_msg(GET_POLICY_REPLY,g_recv_xid,sizeof(struct PolicyReply),(uint8_t *)&policyReply))
        	   {
        	      printf("reply policy failure\n");
        	   }
        	 break;
        case FDT_MOD:
        	trace_ptr =(Trace*)(msg_ptr+sizeof(struct core_header));
        	struct trace_tree* trace;
        	pof_NtoH_transfer_trace(trace_ptr);
        	///////////////
        	struct packet* pkt;///////////////////////////////////????????????????
        	trace=transfer_trace_from_core(pkt,trace_ptr);
        	////////////////////////////////////////////////////lack the switch struct
            /*if(add_trace_to_tree(&(sw->trace_tree),&trace,trace_ptr->num_trace_node))
            {
            	printf("add the trace to the tree successful\n");
            }
            else
            {
            	printf("add the trace failure\n");
            }*/

        	////set the fdtReply

        	pof_HtoN_transfer_FDT_reply(&fdtReply);
        	if(RET_OK!=reply_msg(FDT_REPLY,g_recv_xid,sizeof(struct FDTReply),(uint8_t *)&fdtReply))
        	   {
        	        printf("reply protocol failure\n");
        	    }
            break;
        case ROLE_REQUEST:
        	///////////////////////////
        	break;
        default:
            //POF_ERROR_HANDLE_RETURN_UPWARD(POFET_BAD_REQUEST, POFBRC_BAD_TYPE, g_recv_xid);
            break;
    }
      return RET_OK;
}

//this function is use to add the trace which is from the core to the native_trace_tree.


uint32_t  reply_msg(uint8_t  type,uint32_t xid,uint32_t msg_len,uint8_t*msg_body)
{
    conn_desc *conn_desc_ptr = (conn_desc *)&core_conn_desc;
    struct core_header* header_ptr;
    uint32_t total_len = msg_len + sizeof(struct core_header);

    /* If valid, fetch one message and send it to core. */
    switch(conn_desc_ptr->conn_status){
        case CHANNEL_INVALID:
        case CHANNEL_CONNECTING:
        case CHANNEL_CONNECTED:
        case WAIT_HELLO:
            break;
        case WAIT_FEATURE_REQUEST:
        case WAIT_SET_PROTO:
		case WAIT_SET_FUN:
        case CHANNEL_RUN:

			header_ptr = (struct core_header*)queue_msg_buf;
			header_ptr->version = CORE_VERSION;
			header_ptr->type = type;
			header_ptr->xid = xid;
			header_ptr->length = total_len;

			pof_HtoN_transfer_header(header_ptr);

			if(msg_body != NULL){
				memcpy(queue_msg_buf + sizeof(struct core_header), (uint8_t*)msg_body, msg_len);
			}

			if(RET_OK != send_msg_to_queue(queue_msg_buf, total_len)){

			}
            break;
        default:
            break;
    }

    return RET_OK;
}

uint32_t send_msg_to_queue(uint8_t *msg, uint32_t len){
    if(RET_OK != msg_queue_write(send_queue_id, msg, len, WAIT_FOREVER)){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_WRITE_MSG_QUEUE_FAILURE);
    }

	return RET_OK;
}
bool add_trace_to_tree(struct trace_tree** root,struct trace_tree** trace,int nodeNum)
{
	int i=0;
	struct trace_tree ** t = root;
	struct trace_tree ** t_trace=trace;
	struct trace_tree_T *t_T;
	struct trace_tree_V *t_V;
	//struct trace_tree_D *t_D;
	struct trace_tree_G *t_G;
	struct trace_tree_WM *t_WM;
	struct trace_tree_RM *t_RM;
	struct trace_tree_WMU *t_WMU;
	struct trace_tree_RMU *t_RMU;
	struct trace_tree_T *t_T_dup;
	struct trace_tree_V *t_V_dup;
	//struct trace_tree_D *t_D_dup;
	struct trace_tree_G *t_G_dup;
	struct trace_tree_WM *t_WM_dup;
	struct trace_tree_RM *t_RM_dup;
	struct trace_tree_WMU *t_WMU_dup;
	struct trace_tree_RMU *t_RMU_dup;
	if((*t)->type==TT_E)
	{
		free((*t));
		*t=*t_trace;
		return true;
	}
	for(i=0;i<nodeNum;i++)
	{
	switch((*t)->type){
	case TT_E:
	case TT_L:
		break;
	case TT_D:
		//because the core trace which is not exists the TT_D node.so we should complete the condition
	     break;
	case TT_T:
		t_T=*(struct trace_tree_T**) t;
	    assert((*t_trace)->type==TT_T);
	    t_T_dup=*(struct trace_tree_T**) t_trace;
	    if(t_T_dup->t!=NULL)
	    {
	    	if(t_T->t->type==TT_E)
	    	{
	    		free(t_T->t);
	    		t_T->t=t_T_dup->t;
	    		return true;
	    	}
	    	else
	    	{
	    		t=&(t_T->t);
	    		t_trace=&(t_T_dup->t);
	    	}
	    }
	    else
	    {
	    	if(t_T->f->type==TT_E)
	    	{
	    		free(t_T->f);
	    		t_T->f=t_T_dup->f;
	    		return true;
	    	}
	    	else
	    	{
	    		t=&(t_T->f);
	    		t_trace=&(t_T_dup->f);
	    	}
	    }
	    break;
	case TT_V:
		t_V=*(struct trace_tree_V**)t;
		assert((*t_trace)->type==TT_V);
		t_V_dup=*(struct trace_tree_V**)t_trace;
		int j=0;
		for(j = 0; j < t_V->num_branches; j++) {
			if(value_equal(t_V->branches[j].value,t_V_dup->branches[0].value)) {
				t = &(t_V->branches[j].tree);
				t_trace=&(t_V_dup->branches[0].tree);
				break;
			}
		}
		if(j == t_V->num_branches) {
			t_V->num_branches++;
			*t = realloc(*t,
				     sizeof(struct trace_tree_V) +
				     t_V->num_branches * sizeof(struct trace_tree_Vb));
			t_V = *(struct trace_tree_V **) t;
			t_V->branches[j].value =t_V_dup->branches[0].value;
			t_V->branches[j].tree = t_V_dup->branches[0].tree;
			return true;
		}
	    break;
	case TT_G:
		t_G=*(struct trace_tree_G**)t;
		assert((*t_trace)->type==TT_G);
		t_G_dup=*(struct trace_tree_G**) t_trace;
		if(t_G->t->type==TT_E)
		{
			free(t_G->t);
			t_G->t=t_G_dup->t;
			return true;
		}
		else
		{
			t=&(t_G->t);
			t_trace=&(t_G_dup->t);
		}
		break;
	case TT_WM:
		t_WM = *(struct trace_tree_WM **) t;
		assert((*t_trace)->type == TT_WM);
		t_WM_dup=*(struct trace_tree_WM**) t_trace;
		//t_WM->h.flag_new=1;
		if(t_WM->t->type == TT_E) {
			free(t_WM->t);
			t_WM->t = t_WM_dup->t;
			return true;
		} else {
			t = &(t_WM->t);
			t_trace=&(t_WM_dup->t);
		}
		break;
	case TT_RM:
		t_RM = *(struct trace_tree_RM **) t;
		assert((*t_trace)->type == TT_RM);
		t_RM_dup=*(struct trace_tree_RM**) t_trace;
		//t_RM->h.flag_new=1;
		for(j = 0; j < t_RM->num_branches; j++) {
			if(value_equal(t_RM->branches[j].value, t_RM_dup->branches[0].value)) {
				t = &(t_RM->branches[j].tree);
				t_trace=&(t_RM->branches[0].tree);
				break;
			}
		}
		if(j == t_RM->num_branches) {
			t_RM->num_branches++;
			*t = realloc(*t,
					 sizeof(struct trace_tree_RM) +
					 t_RM->num_branches * sizeof(struct trace_tree_Vb));
			t_RM = *(struct trace_tree_RM **) t;
			t_RM->branches[j].value = t_RM_dup->branches[j].value;
			t_RM->branches[j].tree = t_RM_dup->branches[0].tree;
			return true;
		}
		break;
	case TT_WMU:
		t_WMU = *(struct trace_tree_WMU **) t;
		assert((*t_trace)->type==TT_WMU);
		t_WMU_dup=*(struct trace_tree_WMU**)t_trace;
		//t_WMU->h.flag_new=1;
		if(t_WMU->t->type == TT_E) {
			free(t_WMU->t);
			t_WMU->t = t_WMU_dup->t;
			return true;
		} else {
			t = &(t_WMU->t);
			t_trace=&(t_WMU->t);

		}
		break;
	case TT_RMU:
		t_RMU = *(struct trace_tree_RMU **) t;
		/* ev->type == EV_RMU */
		assert((*t_trace)->type == TT_RMU);
		//t_RMU->h.flag_new=1;
		t_RMU_dup=*(struct trace_tree_RMU**) t_trace;
		for(j = 0; j < t_RMU->num_branches; j++) {
			if(value_equal(t_RMU->branches[j].value, t_RMU_dup->branches[0].value)) {
				t = &(t_RMU->branches[j].tree);
				t_trace=&(t_RMU_dup->branches[0].tree);
				break;
			}
		}
		if(j == t_RMU->num_branches) {
			t_RMU->num_branches++;
			*t = realloc(*t,
					 sizeof(struct trace_tree_RMU) +
					 t_RMU->num_branches * sizeof(struct trace_tree_Vb));
			t_RMU = *(struct trace_tree_RMU **) t;
			t_RMU->branches[j].value = t_RMU_dup->branches[0].value;
			t_RMU->branches[j].tree = t_RMU_dup->branches[0].tree;
			return true;
		}
		break;
	 }
	}
  return true;
}


