/*
 * base_func.h
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "sloop/include/global_var.h"
#include "sloop/include/msg_header.h"
#ifndef POP_METADATA_SLOOP_SOURCE_BASE_FUNC_H_
#define POP_METADATA_SLOOP_SOURCE_BASE_FUNC_H_
uint32_t build_header(struct core_header *header,uint8_t type, uint16_t len, uint32_t xid);
uint32_t connect_config( char *core_ip, uint16_t core_port,uint32_t retry_max,uint32_t retry_interval,uint32_t echo_interval);
uint32_t connect_core(int socket_fd, char *server_ip, uint16_t port);
void produce_deviceFP();
uint32_t get_name_by_ip(char* name,char* ip);
uint32_t get_hwaddr_by_name(const char *name,unsigned char *hwaddr);
uint32_t core_create_socket(int *socket_fd_ptr);
uint32_t echo_timer_create(uint32_t delay,uint32_t interval, TIMER_FUNC timer_handler,uint32_t *timer_id_ptr);
uint32_t echo_timer_delete(uint32_t *timer_id_ptr);
uint32_t echo_timer(uint32_t timer_id, int arg);

#endif /* POP_METADATA_SLOOP_SOURCE_BASE_FUNC_H_ */
