/*
 * nsg_queue.c
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "sloop/Source/msg.h"

#include <sys/msg.h>
#include <unistd.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <unistd.h>

#include "sloop/include/global_var.h"
#include "sloop/include/msg_header.h"
#include "sloop/include/param.h"
#include "sloop/Source/xlog.h"
extern key_t queue_key;
extern conn_desc core_conn_desc;
extern uint32_t cluster_id;
uint32_t msg_queue_create(uint32_t *queue_id_ptr)
{

		uint32_t queue_id = INVALID_QUEUEID;
	    int i = (int)cluster_id;

	    /* Build the queue_key to create the queue. */
	    if(queue_key == 0){
	        queue_key = ftok(".", i);
	    }

	    /* Create the queue using queue_key. */
	    if(-1 == (queue_id = msgget(queue_key,IPC_CREAT|0666))){
	        *queue_id_ptr = INVALID_QUEUEID;
	        printf("Message queue error\n");
	        return ERROR;
	    }
	    xinfo("******msg_queue ID is:%d *******\n",queue_id);
	    queue_key ++;
	    *queue_id_ptr = queue_id;
	    return RET_OK;
}

uint32_t msg_queue_delete(uint32_t *queue_id_ptr){
    uint32_t queue_id = INVALID_QUEUEID;

    if(NULL == queue_id_ptr){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_QUEUE_DELETE_FAIL);
    }
    queue_id = *queue_id_ptr;

    if(queue_id == INVALID_QUEUEID){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_QUEUE_DELETE_FAIL);
    }

    /* Delete the message queue. */
    if(RET_OK != msgctl(queue_id, IPC_RMID, NULL)){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_QUEUE_DELETE_FAIL);
    }

    *queue_id_ptr = INVALID_QUEUEID;

    return RET_OK;
}

uint32_t msg_queue_read(uint32_t queue_id, void *buf, uint32_t max_len, int timeout){
    uint32_t len;
    /* Define the message struct. */
    struct msg{
        long int mtype;
        char mdata[0];
    } *msg_ptr;

    if(queue_id == INVALID_QUEUEID || buf == NULL){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_READ_MSG_QUEUE_FAILURE);
    }

    if(timeout != WAIT_FOREVER && timeout != NO_WAIT){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_WRITE_MSG_QUEUE_FAILURE);
    }

    msg_ptr = MALLOC(max_len + sizeof(struct msg));
    //POF_MALLOC_ERROR_HANDLE_RETURN_NO_UPWARD(msg_ptr);

    /* Receive the messsage from the message queue. */
    if(-1 == (len = msgrcv(queue_id, msg_ptr, max_len + sizeof(struct msg), MSGTYPE_ANY, timeout))){
        FREE(msg_ptr);
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_READ_MSG_QUEUE_FAILURE);
    }

    memcpy(buf, msg_ptr->mdata, len - sizeof(struct msg));

    FREE(msg_ptr);
    return RET_OK;
}

uint32_t msg_queue_write(uint32_t queue_id, const void *message, uint32_t msg_len, int timeout){
    /* Define the message struct. */
    struct msg{
        long int mtype;
        char mdata[0];
    }* msg_ptr;

    if(queue_id == INVALID_QUEUEID || message == NULL){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_WRITE_MSG_QUEUE_FAILURE);
    }

    if(msg_len > MESSAGE_MAX_SIZE){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_MESSAGE_SIZE_TOO_BIG);
    }

    if(timeout != WAIT_FOREVER && timeout != NO_WAIT){
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_WRITE_MSG_QUEUE_FAILURE);
    }

    msg_ptr = MALLOC(msg_len+sizeof(struct msg));
   // POF_MALLOC_ERROR_HANDLE_RETURN_NO_UPWARD(msg_ptr);

    msg_ptr->mtype = MSGTYPE;
    memcpy(msg_ptr->mdata, message, msg_len);

//    msgsnd(queue_id, msg_ptr, msg_len+sizeof(struct msg), IPC_NOWAIT);
    if(-1 == msgsnd(queue_id, msg_ptr, msg_len+sizeof(struct msg), timeout)){
        FREE(msg_ptr);
        //POF_ERROR_HANDLE_RETURN_NO_UPWARD(POFET_SOFTWARE_FAILED, POF_WRITE_MSG_QUEUE_FAILURE);
    }

    FREE(msg_ptr);

    return RET_OK;
}

uint32_t msg_recv(int socket_fd, char* buf, int buflen, int* plen){
	//struct core_header *header_ptr;
    int len;

    if (buflen == 0){
        printf("The length of receive buf is zero.\n");
        return (RECEIVE_MSG_FAILURE);
    }

    if ((len = read(socket_fd, buf, buflen)) <= 0){
        //POF_ERROR_CPRINT_FL("closed socket fd!");
        close(socket_fd);
        return (RECEIVE_MSG_FAILURE);
    }
    *plen = len;

    if(core_conn_desc.conn_status == CHANNEL_RUN){
        return RET_OK;
    }
    return RET_OK;
}


uint32_t msg_send(int socket_fd, char* buf, int len)
{
    int ret;
    /* Send message to core. */
    if ((ret = write(socket_fd, (char *)buf, len) == -1)){

        return (-1);
    }

    return (RET_OK);
}

