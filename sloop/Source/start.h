/*
 * start.h
 *
 *  Created on: Sep 6, 2018
 *      Author: sloop
 */
#include "sloop/include/global_var.h"
#ifndef POP_METADATA_SLOOP_SOURCE_START_H_
#define POP_METADATA_SLOOP_SOURCE_START_H_
int start();
uint32_t destroy();

#endif /* POP_METADATA_SLOOP_SOURCE_START_H_ */
