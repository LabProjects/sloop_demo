/*
 * trace_tree.h
 *
 *  Created on: Aug 4, 2018
 *      Author: sloop
 */

#ifndef INCLUDE_TRACE_TREE_H_
#define INCLUDE_TRACE_TREE_H_
#include "sloop/include/action.h"
enum trace_node_type { TT_E,TT_D,TT_L, TT_V, TT_T, TT_G ,TT_WM,TT_RM,TT_WMU, TT_RMU }; //TT_D is no useful to produce flowtable
struct trace_tree
{
	enum trace_node_type type;
	int flag_new;
};

struct trace_tree_E
{
	struct trace_tree h;
};

struct trace_tree_L
{
	struct trace_tree h;
	struct action *ac;
	int index;
};

struct trace_tree_Vb {
	value_t value;
	struct trace_tree *tree;
};

struct trace_tree_V
{
	struct trace_tree h;
	char name[32];
	int num_branches;
	struct trace_tree_Vb branches[0];
};

struct trace_tree_T
{
	struct trace_tree h;
	char name[32];
	value_t value;
	struct trace_tree *t, *f;
	int barrier_index;
};

struct trace_tree_D
{
	struct trace_tree h;
	char name[32];
	const void *arg;
	struct trace_tree *t;
};

/*
 * milktank
 * trace_tree_WM
 * 2016-03-04
 */
struct trace_tree_WM
{
	struct trace_tree h;
	char name[32];
	int offset;
	int length;
	struct header *spec;
	struct trace_tree *t;
};


struct trace_tree_RM
{
	struct trace_tree h;
	char name[32];
	int num_branches;
	struct trace_tree_Vb branches[0];
};


//Curtis
struct trace_tree_WMU
{
struct trace_tree h;
char name[32];
int length;
value_t value;
struct header *spec;
struct trace_tree *t;
};

struct trace_tree_RMU
{
struct trace_tree h;
char name[32];
int num_branches;
struct trace_tree_Vb branches[0];
};

struct trace_tree_G
{
	struct trace_tree h;
	struct flow_table *ft;
	struct header *old_spec;
	struct header *new_spec;
	int stack_base;
	struct trace_tree *t;
	int index;
};


typedef struct tt_l{
				struct action *ac;
			} tt_l;
typedef struct tt_v{
				char name[32];
				value_t value;
			} tt_v;
typedef struct tt_t{
				char name[32];
				value_t value;
				int flag;
			} tt_t;
typedef struct tt_g{
	   uint8_t flag;
			} tt_g;
typedef struct tt_wm{
				char name[32];
				int offset;
				int length;
				//struct header *spec; don't need;the header is che current header
			} tt_wm;
typedef struct tt_rm{
				char name[32];
				value_t value;
			} tt_rm;
typedef struct tt_wmu{
				char name[32];
				int length;
				value_t value;
			} tt_wmu;
typedef struct tt_rmu{
				char name[32];
				value_t value;
			} tt_rmu;
typedef struct Trace_node {
		enum trace_node_type type;
		union {
			struct {
				struct action *ac;
			} tt_l;
			struct {
				char name[32];
				value_t value;
			} tt_v;
		    struct {
				char name[32];
				value_t value;
				int flag;
			} tt_t;
			struct {
				uint8_t flag;
			}tt_g;
			struct {
				 char name[32];
				 int offset;
				 int length;
							//struct header *spec; don't need;the header is che current header
				} tt_wm;
			struct {
				char name[32];
				value_t value;
				} tt_rm;
			struct {
				char name[32];
				int length;
				value_t value;
				} tt_wmu;
			struct {
				char name[32];
				value_t value;
				} tt_rmu;
		}u;
} trace_node;
typedef struct Trace
{
	int num_trace_node;
	trace_node t[TRACE_NODE_MAX_NUM];
} Trace;


#endif /* INCLUDE_TRACE_TREE_H_ */
