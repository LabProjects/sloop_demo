/*
 * header.h
 *
 *  Created on: Aug 6, 2018
 *      Author: sloop
 */

#ifndef INCLUDE_SPEC_HEADER_H_
#define INCLUDE_SPEC_HEADER_H_
#include "sloop/include/action.h"
 struct header {
	char name[32];
	int num_fields;
	struct {
		char name[32];
		int offset;
		int length;
	} fields[100];
	struct expr *length;
	int header_length;
	int sel_idx;
	int sum_idx;
	int num_next;
	struct {
		value_t v;
		struct header *h;
	} next[100];
};
struct header * spec_header;
#endif /* INCLUDE_SPEC_HEADER_H_ */
