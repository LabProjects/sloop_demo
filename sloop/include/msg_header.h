/*
 * msg_header.h
 *
 *  Created on: Aug 6, 2018
 *      Author: sloop
 */
#ifndef INCLUDE_MSG_HEADER_H_
#define INCLUDE_MSG_HEADER_H_
#include "sloop/include/global_var.h"
struct core_header{
    uint8_t  version; /* CORE_VERSION. */
    uint8_t  type; /* message_type. */
    uint16_t length; /* Length including this header. */
    uint32_t xid; /* Transaction id associated with this packet.
                    Replies use the same id as was in the request to facilitate pairing. */
};        //sizeof=8
struct FDTReply{


};
struct FeatureReply{
	uint32_t nodeId;
	uint32_t nodeRole;
	uint64_t deviceFingerprint;
};
struct ProtocolReply{
	//uint32_t pro_status=pro_status;
	uint32_t pro_status;
	//uint32_t Invalid_padd=0;
	uint32_t Invalid_padd;
};
struct PolicyReply{
	//uint32_t policy_status=policy_status;
	uint32_t policy_status;
	//uint32_t Invalid_padd=0;
	uint32_t Invalid_padd;
};
struct PortReport{
	uint32_t portId;
	uint32_t deviceId;
	uint8_t hwa[CORE_ETH_ALEN];
	//uint16_t Invalid_padd=0;
	uint16_t Invalid_padd;
	uint8_t name[64];
	uint32_t config;
	uint32_t state;
	uint32_t currentFeatures;
	uint32_t advertisedFeatures;
	uint32_t supportedFeatures;
	uint32_t peerFeatures;
	uint32_t currentSpeed;
	uint32_t maxSpeed;
	uint8_t openflowEnable;
	//uint8_t Invalid_padd_1[7]={0};
	uint8_t Invalid_padd_1[7];
};
struct SwitchReport{
	uint32_t switchId;
	enum SwitchReason switchReason;
	uint8_t port_Num;
	//uint16_t Invalid_padd=0;
	uint16_t Invalid_padd;
	struct PortReport port[OFP_MAX_PORTS_NUM_PER_REPORT];
	enum PortReason portReasons[OFP_MAX_PORTS_NUM_PER_REPORT];
	//uint8_t Invalid_padd1[30]={0};
	uint8_t Invalid_padd1[30];
};
struct LinkReport{
	uint32_t srcSwitchId;
	uint32_t srcPortId;
	uint32_t dstSwitchId;
	uint32_t dstPortId;
	enum LinkState linkstate;
	enum LinkType linktype;
	enum LinkReason reason;
	//uint8_t Invalid_padd[5]={0};
	uint8_t Invalid_padd[5];
};
struct HostReport{
	uint32_t attachedSwitchId;
	uint32_t attachedPortId;
	uint32_t hostId;
	uint32_t hostIp;
	enum HostReason reason;
	//uint8_t Invalid_padd[7]={0};
	uint8_t Invalid_padd[7];
};
typedef struct Expression{
	enum expr_type type;
	uint8_t leftFieldId;
	uint8_t rightFieldId;
	uint32_t leftVlaue;
	uint32_t rightValue;
	uint8_t Invald_padd[5];

}Expr;
struct ProtocolHeader{
	uint8_t headerName[32];
	uint8_t headerId;
	uint8_t fieldNum;
	struct Field
	{
		uint8_t fieldName[32];
		uint16_t fieldId;
		uint16_t offset;
		uint16_t length;
		uint8_t isMatch;
		uint8_t isOption;
	}fieldList[10];
	Expr expr_length;
	uint8_t selectFieldId;
	uint8_t checksumField;
	uint8_t selectNum;
	struct Next_Select{
		uint8_t value[16];
		uint8_t headerId;
		uint8_t Invalid_padd[7];
	}selectList[10];
};
struct Set_Protocol
{
	uint8_t protocolName[64];
	uint16_t protocolId;
	uint16_t totalLength;
	uint32_t headerNum;
	struct ProtocolHeader pro_header_list[10];
};
#endif /* INCLUDE_MSG_HEADER_H_ */
