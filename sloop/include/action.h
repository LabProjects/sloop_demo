/*
 * action.h
 *
 *  Created on: Aug 4, 2018
 *      Author: sloop
 */

#ifndef INCLUDE_ACTION_H_
#define INCLUDE_ACTION_H_
#include "sloop/include/pof_type.h"
#include "sloop/include/value.h"
enum action_oper_type {
	AC_OP_ADD,
	AC_OP_SUB,
	AC_OP_AND,
	AC_OP_OR,
	AC_OP_SHL,
	AC_OP_SHR,
	AC_OP_XOR,
	AC_OP_NOR,
};

enum action_type {
	/* "actions" */
	AC_DROP,
	AC_PACKET_IN,
	AC_OUTPUT             /*port*/,
	AC_SET_FIELD,
	AC_ADD_FIELD,
	AC_DEL_FIELD,
	AC_COUNTER,
	/* "instructions" */
	AC_GOTO_TABLE,
	AC_MOVE_PACKET,
	AC_MOVE_PACKET_IMM,
	AC_CALC_R,
	AC_CALC_I,
	AC_WRITE_METADATA,
	AC_CHECKSUM,
	AC_WRITE_METADATA_FROM_PACKET
};

enum move_direction {
	MOVE_FORWARD,
	MOVE_BACKWARD
};
enum match_field_type {
	MATCH_FIELD_PACKET,
	MATCH_FIELD_METADATA
};
#define VALUE_LEN 16
/*typedef struct {
	uint8_t v[VALUE_LEN];
} value_t;*/

#define TRACE_NODE_MAX_NUM 20
#define ACTION_NUM_ACTIONS 18
struct action
{
	int num_actions;
	struct action_entry {
		enum action_type type;
		union {
			int arg;
			struct {
				int tid;
				int offset;
			} goto_table;
			struct {
				enum action_oper_type op_type;
				enum match_field_type dst_type;
				int dst_offset;
				int dst_length;
				enum match_field_type src_type;
				int src_offset;
				int src_length;
			} op_r;
			struct {
				enum action_oper_type op_type;
				enum match_field_type dst_type;
				int dst_offset;
				int dst_length;
				uint32_t src_value;
			} op_i;
			struct {
				int dst_offset;
				int dst_length;
				value_t val;
			} write_metadata;
			//milktank
			struct{
				int dst_offset;
				int dst_length;
				int pkt_offset;
			}write_metadata_from_packet;
			struct {
				enum move_direction dir;
				enum match_field_type type;
				int offset;
				int length;
			} move_packet;
			struct {
				enum move_direction dir;
				int value;
			} move_packet_imm;
			struct {
				int dst_offset;
				int dst_length;
				value_t val;
			} set_field;
			struct {
				int dst_offset;
				int dst_length;
				value_t val;
			} add_field;
			struct {
				int dst_offset;
				int dst_length;
			} del_field;
			struct {
				int sum_offset;
				int sum_length;
				int cal_offset;
				int cal_length;
			} checksum;
		} u;
	} a[ACTION_NUM_ACTIONS];
};
struct action *action_p(void);
struct action *action_copy(struct action *a);
#endif /* INCLUDE_ACTION_H_ */
