/*
 * global_var.h
 *
 *  Created on: Jul 31, 2018
 *      Author: sloop
 */
#include<stdio.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/time.h>
#include <sys/msg.h>

#include "sloop/include/pof_byte_transfer.h"
#include "sloop/include/pof_type.h"
#include "sloop/include/spec_header.h"

#ifndef INCLUDE_GLOBAL_VAR_H_
#define INCLUDE_GLOBAL_VAR_H_
typedef __key_t key_t;
#define CORE_IP_LEN (20)
#define PORT_NAME (64)
#define CORE_ETH_ALEN (6)
#define MESSAGE_MAX_SIZE (2560)
#define INVALID_QUEUEID (-1)
#define VALID_QUEUEID (1)
#define INVALID_TIMERID (0)
#define VALID_TIMERID (1)
#define INVALID_TASKID (0)
#define CORE_VERSION (0x01)
#define CORE_INITIAL_XID (0)
#define WAIT_FOREVER (0)
#define POF_NO_WAIT IPC_NOWAIT
#define ECHO_INTERVAL (2000)
#define NO_WAIT IPC_NOWAIT
#define MSGTYPE_ANY (0)
#define MSGTYPE     (1)
#define MALLOC(size)  malloc(size)
#define FREE(ptr)     free(ptr)
#define CONNECTION_RETRY_INTERVAL (2)
#define CONNECTION_MAX_RETRY_TIME (0XFFFFFFFF)
#define OFP_MAX_PORTS_NUM_PER_REPORT (10)
#define MAX_HEADER_SUM (10)
#define STACK_TOP(pp) ((pp)->stack[(pp)->stack_top])
/* Define max size of receiving buffer. */
#define RECV_BUF_MAX_SIZE (MESSAGE_MAX_SIZE)
typedef pthread_t task_t;
typedef void (*TIMER_FUNC)(uint32_t timerid, int arg);
typedef void (*TASK_FUNC)(void *arg_ptr);
enum expr_type{
	EXPR_FIELD,
	EXPR_VALUE,
	EXPR_NOT,
	EXPR_ADD,
	EXPR_SUB,
	EXPR_SHL,
	EXPR_SHR,
	EXPR_AND,
	EXPR_OR,
	EXPR_XOR
};
struct expr {
	enum expr_type type;
	union {
		struct expr *sub_expr;
		struct {
			struct expr *left;
			struct expr *right;
		} binop;
		uint32_t value;
		char field[32];
	} u;
}; //true expr;
typedef enum error_type{
    RET_OK = 0,
	ERROR=0x5001,
	QUEUE_ERROR =0x5002,
	TASK_ERROR =0X5003,
	CONNECT_SERVER_FAILURE =0x5004,
	CREATE_SOCKET_FAILURE =0x5005,
	RECEIVE_MSG_FAILURE =0x5006
} core_error;
typedef struct conn_desc{
    /* core information. */
    char core_ip[CORE_IP_LEN];  /* Ipv4 address of core. */
    uint16_t core_port;
    uint8_t local_port;
    /* Connection socket id and socket buffers. */
    int sfd; /* Scket id. */
    char send_buf[MESSAGE_MAX_SIZE];
    char recv_buf[MESSAGE_MAX_SIZE];
    char msg_buf[MESSAGE_MAX_SIZE];
    /*connect status*/
    uint16_t conn_status;
    time_t last_echo_time;
    uint32_t echo_interval;
    uint32_t retry_count;
    uint32_t retry_max;
    uint32_t retry_interval;
}  conn_desc;
typedef enum{
    CHANNEL_INVALID         = 0,
    CHANNEL_CONNECTING      = 1,
    CHANNEL_CONNECTED     	= 2,
	WAIT_HELLO              = 3,
	WAIT_FEATURE_REQUEST    =4,
    WAIT_SET_PROTO          = 5,
    WAIT_SET_FUN            = 6,
    CHANNEL_RUN             = 7,
    POFCS_STATE_MAX         = 8,
	CHANNEL_WRONG_STATE     = 9
} channel_state;
/*define the message type*/
typedef enum core_type {
    /* Immutable messages. */
	HELLO                          =0,
    MSG_ERROR                      =1,
	ECHO_REQUEST                   =2,
	ECHO_REPLY                     =3,

	/* Cluster information messages. */
	FEATURE_REQUEST		           =4,
	FEATURE_REPLY      			   =5,
	SET_PROTOCOL                   =6,
	GET_PROTOCOL_REPLY	           =7,
	SET_POLICY      	           =8,
	GET_POLICY_REPLY	           =9,
	SWITCH_REPORT 		           =10,
	LINK_REPORT 		           =11,
	HOSTS_REPORT    	           =12,
	/* Cluster notification messages. */
	PACKET_IN                      =13,
	FDT_REPLY        			   =14,
	LLDP_REPORT                    =15,
	PACKET_OUT                     =16,
	FDT_MOD                        =17,
	/* Controller role request messages. */
	/**
	  * send from the SLOOP to cluster node for identifying the node role
	*/
	ROLE_REQUEST                   =18,
	ROLE_REPLY                     =19

 }core_type;
 typedef enum HPROLE{
	 HP_POP         = 1 << 0,
	 HP_SEC         = 1 << 1
 }HPROLE;
 typedef enum ProtocolStatus{
	 SUCCESS		= 1 << 0,
	 FAILED			= 1 << 1,
	 UNKNOWN        = 1 << 2
 }ProtocolStatus;


 //Host Reason
  enum HostReason{
	 HOST_ADD,
	 HOST_DELETE,
	 HOST_UPDATE
 };

 //Link Type
 enum LinkType{
	 DIRECT,
	 INDIRECT,
	 EDGE
 };

 //Link State
enum LinkState{
	 ACTIVE,
	 INACTIVE
 };

 //Link Reason

 enum LinkReason{
	 LINK_ADD,
	 LINK_DELETE,
	 LINK_UPDATE

 };

 //Switch Reason

enum SwitchReason{
	 OFPPR_CONNECT,
	 OFPPR_DISCONNECT,
	 OFPPR_HOLD
 };

 //Port Reason

enum PortReason{
	 OFPPR_ADD,
	 OFPPR_DELETE,
	 OFPPR_MODIFY
 };


 struct packet {
 	struct packet_parser *pp;
 	struct xswitch *in_sw;
 	int in_port;
 	bool hack_get_payload;
 };
 struct packet_parser
 {
 	struct {
 		struct header *spec;
 		uint8_t *data;
 		int length;
 	} stack[32];
 	int stack_top;
 	int metadata_offset;
 };
#endif /* INCLUDE_GLOBAL_VAR_H_ */
