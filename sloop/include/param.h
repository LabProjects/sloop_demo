/*
 * param.h
 *
 *  Created on: Sep 4, 2018
 *      Author: sloop
 */
#include "sloop/include/global_var.h"
#include "sloop/include/spec_header.h"
#ifndef INCLUDE_PARAM_H_
#define INCLUDE_PARAM_H_
extern task_t main_task_id;
extern task_t send_msg_task_id ;
extern char queue_msg_buf[MESSAGE_MAX_SIZE];
extern char *DEFAULT_CORE_IP;
extern uint32_t DEFAULT_CORE_PORT;
extern uint32_t conn_max_retry;

/* The retry interval of cnnection if connect fails. */
extern uint32_t conn_retry_interval;
extern conn_desc core_conn_desc;
extern uint32_t send_queue_id;
extern uint32_t cluster_id;
extern uint64_t deviceFP;
extern key_t queue_key;
extern uint32_t g_upward_xid;
extern uint32_t echo_interval;
extern uint32_t echo_timer_id;
extern uint32_t policy_status;
extern uint32_t pro_status;
extern struct header * headerList[MAX_HEADER_SUM];


#endif /* INCLUDE_PARAM_H_ */
